﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.IO.Compression;
using B2C_ACCOOUNTS_MANAGER.Functions;
using B2C_ACCOOUNTS_MANAGER.Models;
using B2C_ACCOOUNTS_MANAGER.Models.UOW;
using PagedList;

namespace B2C_ACCOOUNTS_MANAGER.Controllers
{
    [Authorize]
    public class PaymentTransactionController : Controller
    {
        private readonly Repository _repository;
        private readonly UniqueId _uniqueId = new UniqueId();
        private const int PageSize = 25;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        B2C_MANAGEREntities db = new B2C_MANAGEREntities();
        // GET: PaymentTransaction
        public ActionResult Index(string currentFilter, string Status, string search, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            //PaymentViewModel model = new PaymentViewModel();
            
            if (Status == "org_name")
            {
                var org_details = db.Organizations.OrderByDescending(s => s.ID).FirstOrDefault(s => s.org_name.StartsWith(search));

                string code = org_details.org_code.ToString();

                var ps = (from x in db.PaymentTrans
                         where (x.Approve_Status != 3 && x.Approve_Status != 2 && x.org_code == code)
                         select new PaymentViewModel
                         {
                             Status_Desc = (from xx in db.Status_Options where xx.ID == x.Approve_Status select xx.Status).ToList(),
                             ID = x.ID,
                             orgcode = x.org_code,
                             TransDate = x.TransDate,
                             //ChequeNo = x.ChequeNo,
                             Amount = x.Amount,
                             Status = x.Approve_Status,
                         }).ToList().OrderByDescending(c => c.ID);
                return View(ps.ToPagedList(pageNumber, pageSize));
            }
            //var p = from s in db.PaymentTrans join sa in db.Status_Options on s.Approve_Status equals sa.Status select s;
            var p = (from x in db.PaymentTrans
                     where (x.Approve_Status != 3 && x.Approve_Status != 2)
                     select new PaymentViewModel
                     {
                         Status_Desc = (from xx in db.Status_Options where xx.ID == x.Approve_Status select xx.Status).ToList(),
                         ID = x.ID,
                         orgcode = x.org_code,
                         TransDate = x.TransDate,
                         //ChequeNo = x.ChequeNo,
                         Amount = x.Amount,
                         Status = x.Approve_Status,
                     }).ToList().OrderByDescending(c => c.ID);

            return View(p.ToPagedList(pageNumber, pageSize));
            //var payments = db.PaymentTrans.Where(p => p.Approve_Status != 3 && p.Approve_Status !=2).ToList();

            //foreach (var n in payments)
            //{
            //    var model = new PaymentViewModel
            ////    {
            //        orgcode = n.org_code,
            //        TransDate = n.TransDate,
            //        ChequeNo = n.ChequeNo,
            //        Amount = n.Amount
            //    };    
            //}
            //return View(p);
        }
        public ActionResult Details(int id)
        {
            PaymentTran tran = db.PaymentTrans.Single(x => x.ID == id);
            if (tran.Chequeimg == null)
            {
                return RedirectToAction("Det", new { id = id });
            }
            else
            {
                int org = Convert.ToInt32(tran.org_code);
                var orgname = db.Organizations.Single(p => p.org_code == org);
                string org_name = orgname.org_name;
                ViewBag.Name = org_name;
                return View(tran);
            }
        }
        public ActionResult Det(int id)
        {
            ViewBag.Logo = "D:/CODE/B2C/B2C_ACCOOUNTS_MANAGER/B2C_ACCOOUNTS_MANAGER/PaymentTransaction/Detphotos/noimage.png";
            //Server.MapPath("~") + @"photos\noimage.png";
            PaymentTran tran = db.PaymentTrans.Single(x => x.ID == id);
            int org = Convert.ToInt32(tran.org_code);
            var orgname = db.Organizations.Single(p => p.org_code == org);
            string org_name = orgname.org_name;
            ViewBag.Name = org_name;
            return View(tran);
        }
        public ActionResult Approve(long id, PaymentTran trans)
        {

            PaymentTran tran = db.PaymentTrans.Find(id);
            tran.Approve_Status = Convert.ToInt32(MyEnums.StatusOption.Approved);
            db.Entry(tran).State = EntityState.Modified;
            db.SaveChanges();

            int orgcode = Convert.ToInt32(tran.org_code);
            AmountBal bal = new AmountBal();

            decimal ballance = Convert.ToDecimal(tran.Amount);
            var a_bal = db.AmountBals.Single(x => x.org_code == orgcode);
            decimal cash = a_bal.balance;
            decimal total = cash + ballance;
            a_bal.balance = total;
            db.Entry(a_bal).State = EntityState.Modified;
            db.SaveChanges();

            var pay_report = db.Saf_Reports.OrderByDescending(p => p.ID).FirstOrDefault(p => p.Org_Code == orgcode);

            string other_info = tran.ChequeNo + "_" + "Account Credit";
            
            if (pay_report == null)
            {
                Saf_Reports report = new Saf_Reports();
                report.Org_Code = orgcode;
                report.Receipt_No = tran.ChequeNo;
                report.Details = "Account Credit";
                report.Transaction_Status = "Completed";
                report.PaidIn = tran.Amount.ToString();
                report.Balance = tran.Amount;
                report.Reason_Type = "Account Credit";
                report.Other_Info = other_info;
                report.Initiation_Time = DateTime.Now;
                report.Completion_Time = DateTime.Now;
                report.Widthrawn = Convert.ToDecimal(0.00);
                db.Saf_Reports.Add(report);
                db.SaveChanges();
            }
            else
            {
                decimal? amount_added = tran.Amount;
                decimal? pay_amount = pay_report.Balance;
                decimal? tot = amount_added + pay_amount;

                Saf_Reports report = new Saf_Reports();
                report.Org_Code = orgcode;
                report.Receipt_No = tran.ChequeNo;
                report.Details = "Account Credit";
                report.Transaction_Status = "Completed";
                report.PaidIn = tran.Amount.ToString();
                report.Balance = tot;
                report.Reason_Type = "Account Credit";
                report.Other_Info = other_info;
                report.Initiation_Time = DateTime.Now;
                report.Completion_Time = DateTime.Now;
                report.Widthrawn = Convert.ToDecimal(0.00);
                db.Saf_Reports.Add(report);
                db.SaveChanges();
            }
            TempData["shortMessage1"] = "Successfully Approved";
            return RedirectToAction("Index");
        }

        public ActionResult Unapprove(long id, FormCollection form)
        {
            PaymentTran tran = db.PaymentTrans.Find(id);
            tran.Approve_Status = Convert.ToInt32(MyEnums.StatusOption.Unapproved);
            db.Entry(tran).State = EntityState.Modified;
            db.SaveChanges();
            TempData["shortMessage1"] = "Successfully Unapproved";
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            var model = db.PaymentTrans.Find(id);
            //
            
            //model.Approve_Status = Convert.ToInt32(MyEnums.StatusOption.Deleted);
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();
            TempData["shortMessage1"] = "Successfully Deleted";
            return RedirectToAction("Index");   
        }
    }
}