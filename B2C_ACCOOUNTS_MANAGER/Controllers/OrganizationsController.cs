﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using B2C_ACCOOUNTS_MANAGER.Models;
using PagedList;

namespace B2C_ACCOOUNTS_MANAGER.Controllers
{
    [Authorize]
    public class OrganizationsController : Controller
    {
        B2C_MANAGEREntities db = new B2C_MANAGEREntities();
        // GET: Organizations
       
        public ActionResult Index(string currentFilter, string Status, string search, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var orgs = db.Organizations.ToList();
            if (Status == "org_name")
            {
                orgs = orgs.Where(s => s.org_name.StartsWith(search)).ToList();
                return View(orgs.ToPagedList(pageNumber, pageSize));
            }
            return View(orgs.ToPagedList(pageNumber, pageSize));
        }
      
        public ActionResult Create()
        {
            return View();
        }
       
        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            Organization org = new Organization();
            if (ModelState.IsValid)
            {

                var org_code = db.Organizations.OrderByDescending(p => p.org_code).FirstOrDefault();
                if (org_code == null)
                {
                    org.org_code = 1;
                }
                else
                {
                    int code = org_code.org_code;
                    int organisation1 = code + 1;
                    org.org_code = organisation1;
                }
                org.org_name = form["org_name"];
                db.Organizations.Add(org);
                db.SaveChanges();


                AmountBal bal = new AmountBal();
                bal.org_code = org.org_code;
                bal.balance = 0;
                db.AmountBals.Add(bal);
                db.SaveChanges();
                TempData["shortMsg"] = "Created Successsfully";
                return RedirectToAction("Index");

            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization org = db.Organizations.Find(id);
            
            if (org == null)
            {
                return HttpNotFound();
            }
            return View(org);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID, org_code, org_name")]Organization org)
        {
            if (ModelState.IsValid)
            {
                db.Entry(org).State = EntityState.Modified;
                db.SaveChanges();
                TempData["shortMessage"] = "Edited Successsfully";
                return RedirectToAction("Index");
            }
            return View(org);

        }
        public ActionResult Delete(int id)
        {
            Organization org = db.Organizations.Find(id);
            db.Organizations.Remove(org);
            db.SaveChanges();
            AmountBal bal = db.AmountBals.OrderByDescending(p => p.ID).FirstOrDefault(p => p.org_code == id);
            db.AmountBals.Remove(bal);
            db.SaveChanges();
            TempData["DeleteMessage"] = "Deleted Successsfully";
            return RedirectToAction("Index");
        }
    }
}