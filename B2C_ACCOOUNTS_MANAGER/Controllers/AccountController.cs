﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using B2C_ACCOOUNTS_MANAGER.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using B2C_ACCOOUNTS_MANAGER.Functions;
using B2C_ACCOOUNTS_MANAGER.Models.UOW;
using PagedList;
using B2C_ACCOOUNTS_MANAGER.PDF;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Font = iTextSharp.text.Font;
using Image = iTextSharp.text.Image;
using System.Text.RegularExpressions;

namespace B2C_ACCOOUNTS_MANAGER.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        readonly Logs _logs = new Logs();
        private readonly Repository _repository;
        private readonly UniqueId _uniqueId = new UniqueId();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        B2C_MANAGEREntities db = new B2C_MANAGEREntities();
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index(string currentFilter, string Status, string search, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var user_det = UserManager.Users.ToList();
            if (Status == "UserName")
            {
                user_det = user_det.Where(s => s.UserName.StartsWith(search)).ToList();
                    //user_det.Where(s => s.UserName.StartsWith(search));
                return View(user_det.ToPagedList(pageNumber, pageSize));
            }
            if(Status == "ID_No")
            {
                user_det = user_det.Where(s => s.ID_No == search).ToList();
                return View(user_det.ToPagedList(pageNumber, pageSize));
            }
                
            var users = UserManager.Users.ToList();
            return View(users.ToPagedList(pageNumber, pageSize));
        }
        
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {

                var result = await SignInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, shouldLockout: false);
                string succ = result.ToString();
                //switch (result)
                //{
                    
                //    case SignInStatus.Success:
                //        return RedirectToAction("Index");
                //    case SignInStatus.LockedOut:
                //        return View("Lockout");
                //    case SignInStatus.RequiresVerification:
                //        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                //    case SignInStatus.Failure:
                //    default:
                //        ModelState.AddModelError("", "Invalid login attempt.");
                //        return View(model);
                //}
                var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == model.Username);
                //admin
                if (user_details.privilege == 0 && succ == "Success" && user_details.Status ==2)
                {
                    return RedirectToAction("Index", "Account");
                }
                //sacco admin
                if (user_details.privilege == 2 && succ == "Success" && user_details.Status == 2)
                {
                    return RedirectToAction("Index", "Payments", new { area = "Sacco" });
                }
                //sacco user
                if(user_details.privilege == 4 && succ == "Success" && user_details.Status == 2)
                {
                    return RedirectToAction("Index", "Registration", new { area = "SaccoUser" });
                }

            }
            catch(Exception ex)
            {

            }
            return View();
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true

        }

        //
        // GET: /Account/Register
        
        public ActionResult Register()
        {
            B2C_MANAGEREntities db = new B2C_MANAGEREntities();
            ViewBag.organisations = new SelectList(db.Organizations, "org_code", "org_name");
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
       
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            string P_number;
            if (ModelState.IsValid)
            {
                try
                {
                    var ussr = db.AspNetUsers.Any(p => p.UserName == model.UserName);

                    if (ussr != false)
                    {
                        TempData["errmessage"] = "User Already Exists!!!";
                        return RedirectToAction("Register");
                    }
                    else
                    {
                        string phone = model.PhoneNumber;
                        if(phone.StartsWith("+"))
                        {
                            string phoneno = phone.Substring(1);
                            P_number = phoneno;
                        }
                        else
                        {
                            string no = phone.Substring(1);
                            string phone_no = "254" + no;
                            P_number = phone_no;
                        }
                        
                        
                        ApplicationDbContext context = new ApplicationDbContext();
                        UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
                        UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
                        string newPassword = model.Password;
                        String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
                        if (ModelState.IsValid)
                        {
                            AspNetUser usr = new AspNetUser();
                            Guid id = _uniqueId.GetId();
                            usr.Id = id.ToString();
                            usr.Names = model.Names;
                            usr.UserName = model.UserName;
                            usr.PhoneNumber = P_number;
                            usr.ID_No = model.IDNO;
                            usr.org_code = model.Org_Code;
                            usr.Email = model.Email;
                            usr.PasswordHash = hashedNewPassword;
                            usr.SecurityStamp = _uniqueId.GetId().ToString();
                            usr.Status = (int)MyEnums.StatusOption.Added;
                            usr.privilege = 2;
                            db.AspNetUsers.Add(usr);
                            db.SaveChanges();
                            TempData["successmessage"] = "Saved successfully";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        public ActionResult ResetPassword()
        {
            string user = Common.Classes.Common.ReturnLastUpdateby(false, Request);
            ViewBag.user = user;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model, FormCollection form)
        {
            string usr = form["txtTitle"].ToString();
            ApplicationDbContext context = new ApplicationDbContext();
            UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);

            var user = await UserManager.FindAsync(usr, model.OldPassword);

            if (user != null)
            {
                string newPassword = model.Password;
                String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
                if (ModelState.IsValid)
                {
                    var useredit = db.AspNetUsers.FirstOrDefault(p => p.UserName == usr);
                    useredit.PasswordHash = hashedNewPassword;
                    db.Entry(useredit).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    TempData["shortM"] = "Successfuly Reset Password";
                    return RedirectToAction("Login");
                }
            }
            TempData["shortMessage1"] = "Invalid username or password";
            return RedirectToAction("ResetPassword");
        }
        public ActionResult UserActivate(string id)
        {
            try
            {
                //var user = UserManager.FindById(id);
                //user.Status = MyEnums.StatusOption.Approved;
                //var xyz = UserManager.Update(user);
                AspNetUser user = db.AspNetUsers.Find(id);
                user.Status = (int)MyEnums.StatusOption.Approved;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                TempData["activatemessage"] = "Activated successfully";
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        public ActionResult UserDeactivate(string id)
        {
            try
            {
                //var user = UserManager.FindById(id);
                //user.Status = MyEnums.StatusOption.Unapproved;
                //var xyz = UserManager.Update(user);
                AspNetUser user = db.AspNetUsers.Find(id);
                user.Status = (int)MyEnums.StatusOption.Unapproved;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                TempData["deactivatemessage"] = "Deactivated successfully";
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        public ActionResult Delete(string id)
        {
            AspNetUser user = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(user);
            db.SaveChanges();
            TempData["deletemessage"] = "Deleted successfully";
            return RedirectToAction("Index");
        }
        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}