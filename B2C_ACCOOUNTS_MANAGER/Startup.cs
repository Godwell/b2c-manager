﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(B2C_ACCOOUNTS_MANAGER.Startup))]
namespace B2C_ACCOOUNTS_MANAGER
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
