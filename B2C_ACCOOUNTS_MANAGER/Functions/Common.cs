﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Xml.Linq;
using B2C_ACCOOUNTS_MANAGER.Models;
using System.Data.Entity;
using System.Text.RegularExpressions;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Common.Classes
{
    public class Common
    {

        public static string ReturnUserIP(HttpRequestBase request)
        {
            try
            {
                //HttpContext.Request.UserHostAddress
                var userHostAddress = request.UserHostAddress;

                IPAddress.Parse(userHostAddress);

                var xForwardedFor = request.ServerVariables["X_FORWARDED_FOR"];

                if (string.IsNullOrEmpty(xForwardedFor))
                    return userHostAddress;

                //get list of public addresses
                var publicForwardingIPs = xForwardedFor.Split(',').Where(ip => !IsPrivateIP(ip)).ToList();

                //return last public ip found / user host address
                return publicForwardingIPs.Any() ? publicForwardingIPs.Last() : userHostAddress;
            }
            catch (Exception)
            {
                return "0.0.0.0";
            }
        }
        public string RandomOTP()
        {
            // declare array string to generate random string with combination of small,capital letters and numbers
            char[] charArr = "0123456789".ToCharArray();

            string strrandom = string.Empty;
            Random objran = new Random();
            int noofcharacters = Convert.ToInt32(4);
            for (int i = 0; i < noofcharacters; i++)
            {
                //It will not allow Repetation of Characters
                int pos = objran.Next(1, charArr.Length);
                if (!strrandom.Contains(charArr.GetValue(pos).ToString()))
                    strrandom += charArr.GetValue(pos);
                else
                    i--;
            }
            return strrandom;
        }
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        public class ValidateFileAttribute : ValidationAttribute
        {
            public override bool IsValid(object value)
            {
                int maxContent = 1024 * 1024; //1 MB
                string[] sAllowedExt = new string[] { ".AGN", ".PAR", ".TXT", ".P3P", "P3A", ".BMP", ".PGN", "M44" };


                var file =  value as HttpPostedFileBase;

                if (file == null)
                    return false;
                else if (!sAllowedExt.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                {
                    ErrorMessage = "Please upload Your Files " + string.Join(", ", sAllowedExt);
                    return false;
                }
                else if (file.ContentLength > maxContent)
                {
                    ErrorMessage = "Your Photo is too large, maximum allowed size is : " + (maxContent / 1024).ToString() + "MB";
                    return false;
                }
                else
                    return true;
            }
        }
        private static bool IsPrivateIP(string ipAddress)
        {

            //Private IPs {24-bit block: 10.0.0.0 - 10.255.255.255; 20-bit block: 172.16.0.0 - 172.31.255.255; 16-bit block: 192.168.0.0 - 192.168.255.255}
            //Link-local IPs {169.254.0.0 through 169.254.255.255}
            var ip = IPAddress.Parse(ipAddress);
            var octets = ip.GetAddressBytes();

            var is24Bit = octets[0] == 10;
            if (is24Bit) return true;

            var is20Bit = octets[0] == 172 && octets[1] >= 16 && octets[1] <= 31;
            if (is20Bit) return true;

            var is16Bit = octets[0] == 192 && octets[1] == 168;
            if (is16Bit) return true;

            var isLinkLocal = octets[0] == 169 && octets[1] == 254;
            return isLinkLocal;

        }

        //upload file
        public static string uploadandreturnpath(HttpPostedFileBase file, string s_folder, string s_newfilename = "", bool backend = false)
        {
            //image upload
            string s_return = "";

            if (file.ContentLength > 0)
            {
                int MaxContentLength = 1024 * 1024 * 50; //50 MB
                string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".JPG", ".GIF", ".PNG" };

                //can upload excel sheets from the backend
                if (backend == true) AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".JPG", ".GIF", ".PNG", ".xls", ".xlsx",".XLS", ".XLSX" };

                if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                {
                    //ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                    return "Error: Please file of type: " + string.Join(", ", AllowedFileExtensions);
                }
                else if (file.ContentLength > MaxContentLength)
                {
                    //ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                    return "Error: Your file is too large, maximum allowed size is: " + MaxContentLength + " MB";
                }
                else
                {
                    try
                    {
                        // extract only the fielname             
                        var fileName = Path.GetFileName(file.FileName);

                        if (s_newfilename != "")
                        {
                            string _fileext = Path.GetExtension(file.FileName);

                            fileName = s_newfilename + _fileext;
                        }

                        //upload path
                        var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("/Content/" + s_folder + "/"), fileName);

                        // this is the string you have to save in your DB
                        string filepathToSave = "/Content/" + s_folder + "/" + fileName;
                        file.SaveAs(path);

                        s_return = filepathToSave;
                    }
                    catch (Exception e)
                    {
                        //ModelState.AddModelError("uploadError", e);
                        //return "Error: " + e.ToString();
                        return "Error uploading image" + e.ToString(); ;
                    }
                }
            }
            return s_return;
        }
        public static string deletefile(string s_filepath)
        {
            string _return = "";

            string s_fullpath = HttpContext.Current.Request.MapPath(s_filepath);

            if (File.Exists(s_fullpath))
            {
                File.Delete(s_fullpath);
                _return = "success";
            }

            return _return;
        }
        public static string renamefile(string s_filepath, string s_newfilename)
        {
            string _return = "";

            string s_fullpath = HttpContext.Current.Request.MapPath(s_filepath);

            if (File.Exists(s_fullpath))
            {
                string _originalfilename = Path.GetFileName(s_filepath);
                string _fileext = Path.GetExtension(_originalfilename);

                string _filepath = Path.GetDirectoryName(s_filepath);
                string _newfullpath = _filepath + "\\" + s_newfilename + _fileext;
                _return = _newfullpath;

                _newfullpath = HttpContext.Current.Request.MapPath(_newfullpath);
                //check if file exists
                if (!File.Exists(_newfullpath)) File.Move(s_fullpath, _newfullpath);
            }

            return _return;
        }

        public static string ReturnLastUpdateby(bool WithIP, HttpRequestBase request)
        {
            string s_return = "";

            //Get Logged In User
            s_return = HttpContext.Current.User.Identity.Name.ToString();

            //WithIP = true | Get User IP
            if (WithIP)
            {
                s_return += " | " + ReturnUserIP(request);
            }
            return s_return;
        }

        //calculate ranking
        //public async Task UpdateCompanyRaking()
        //public static void UpdateCompanyRaking(HttpRequestBase request)
        //{
        //    TP_DataEntities db = new TP_DataEntities();

        //    int _index = 1;
        //    int _featureid = Convert.ToInt32((from f in db.t_systemfeatures where f.f_identifier == "premiumlisting" select f.f_id).FirstOrDefault());

        //    //deactivate all expired subscriptions

        //    //define array to sort; populate array
        //    var _sortlist = (from c in db.t_companies
        //                     let bpcount = (from bp in db.t_blogposts where bp.bp_companyid == c.c_id select bp.bp_id).Count()
        //                     let ispremium = (from pf in db.t_planssystemfeatures where pf.pf_planid == c.c_planid && pf.pf_systemfeatureid == _featureid select pf.pf_planid)
        //                     where ispremium.Contains(c.c_planid)
        //                     orderby c.c_name
        //                     orderby bpcount descending
        //                     select c);
           
        //    //premium sort
        //    foreach (var item in _sortlist.ToList())
        //    {
        //        //update record
        //        //c_planenddate, c_planstartdate
        //        item.c_ranking = _index;
        //        db.Entry(item).State = EntityState.Modified;
        //        _index++;
        //    }
        //    db.SaveChanges();

        //    //for non premiums sort alphabetically
        //    _sortlist = (from c in db.t_companies
        //                 let ispremium = (from pf in db.t_planssystemfeatures where pf.pf_planid == c.c_planid && pf.pf_systemfeatureid == _featureid select pf.pf_planid)
        //                 where !ispremium.Contains(c.c_planid)
        //                 orderby c.c_name
        //                 select c);
        //    foreach (var item in _sortlist)
        //    {
        //        //update record
        //        item.c_ranking = _index;
        //        db.Entry(item).State = EntityState.Modified;
        //        _index++;
        //    }
        //    db.SaveChanges();

        //    //log this activity in the logs table
        //    RecordLog("Company Rakinkings Updated", "ranker", request);
        //}

        ////public static void UpdateFilterRaking(int? id)
        ////{
        ////    TP_DataEntities db = new TP_DataEntities();

        ////    int _skipidex = 0;
        ////    int _index = 1;

        ////    //define array to sort
        ////    List<t_filters> _sortlist = new List<t_filters>();
        ////    //populate array
        ////    _sortlist = db.t_filters.ToList().OrderBy(o => o.ft_rank).ToList();
        ////    //remove updated record from the array
        ////    if (id != null)
        ////    {
        ////        var _skiprecord = db.t_filters.Find(id);
        ////        if (_skiprecord != null)
        ////        {
        ////            //remove from array
        ////            _skipidex = (int)_skiprecord.ft_rank;
        ////            _sortlist.Remove(_skiprecord);
        ////        }
        ////    }
        ////    //update all other records
        ////    foreach (var item in _sortlist)
        ////    {
        ////        //increase counter on skipped record
        ////        if (_index == _skipidex)
        ////            _index = _index + 1;

        ////        //update record if rank does not match index
        ////        if (item.ft_rank != _index)
        ////        {
        ////            //update record
        ////            item.ft_rank = _index;
        ////            db.Entry(item).State = EntityState.Modified;
        ////            db.SaveChanges();
        ////        }
        ////        //increment counter
        ////        _index++;
        ////    }
        ////}

        //record a log entry
        //public static void RecordLog(string logentry, string logtype, HttpRequestBase request, int notifyadmin = 0)
        //{
        //    //logtypes: newscrawl, payment
        //    TP_DataEntities db = new TP_DataEntities();
        //    ////Record Transaction Log
        //    ////t_transactionslog t_transactionslog = new Areas.PaySecAdmin.t_transactionslog();
        //    t_logs _logtable = new t_logs();
        //    //clean the string
        //    string s_logtext = logentry.Replace("<", "\\");
        //    s_logtext = logentry.Replace(">", "//");

        //    //trim text
        //    if (s_logtext != "")
        //        if (s_logtext.Length > 255)
        //            s_logtext = s_logtext.Substring(0, 255);

        //    if (logtype != "")
        //        if (logtype.Length > 10)
        //            logtype = logtype.Substring(0, 10);

        //    //lognotify = 10

        //    //for payment logs: //s_logtext = logentry.Replace(s_cardnumber, "xxxx");
        //    _logtable.log_text = s_logtext;
        //    _logtable.log_type = logtype;
        //    _logtable.log_timestamp = DateTime.Now;
        //    _logtable.log_lastupdate = DateTime.Now;
        //    _logtable.log_lastupdateby = Common.ReturnLastUpdateby(false, request);
        //    if (notifyadmin == 1) _logtable.log_notify = "1";

        //    db.t_logs.Add(_logtable);
        //    db.SaveChanges();
        //}

        //generate invoice 
        //public static string GenerateUserInvoice(int planid, string userid, DateTime startdate, HttpRequestBase request)
        //{
        //    TP_DataEntities db = new TP_DataEntities();
        //    string _return = "fail";

        //    decimal _billedamount = 0;
        //    string _planname = "";
        //    string _pendingstatus = Constants.InvoiceStatus.Pending.ToString();
        //    int _planvaliddays = 0;
        //    int _companyid = 0;

        //    var _plandata = (from p in db.t_plans
        //                     where p.pl_id == planid
        //                     select new PlansViewModel()
        //                     {
        //                         FeaturesinPlan = (from pf in db.t_planssystemfeatures where pf.pf_planid == p.pl_id select pf.t_systemfeatures.f_title).ToList(),
        //                         Plancost = p.pl_cost,
        //                         Plandescription = p.pl_description,
        //                         Planid = p.pl_id,
        //                         ValidDays = p.pl_validdays ?? 0,
        //                         Planname = p.pl_name
        //                     }
        //              ).FirstOrDefault();
        //    if (_plandata != null)
        //    {
        //        _billedamount = ReturnNumbersOnly(_plandata.Plancost);
        //        _planname = _plandata.Planname;
        //        _planvaliddays = _plandata.ValidDays;
        //    }

        //    //generate invoice :: PLANID-DATETIME-PLANNAME :: {2-06102015211055-UPGRADE 2}
        //    var _invoiceref = planid + ReturnDateTimeSingleString() + _planname;   //plan name max: 255

        //    if (_invoiceref.Length > 255) _invoiceref = _invoiceref.Substring(0, 255);

        //    var _settings = db.t_systemsettings.ToDictionary(key => key.sys_param, val => val.sys_value);
        //    int _Invoice_Valid_Days = 0;
        //    int _Invoice_Due_Days = 0;
        //    int _invoicetaxrate = 0;
        //    try
        //    {
        //        _Invoice_Valid_Days = (int)ReturnNumbersOnly(_settings["Invoice_Valid_Days"].ToString());       //default: subscription contract duration if missing in the plan
        //        _Invoice_Due_Days = (int)ReturnNumbersOnly(_settings["Invoice_Due_Days"].ToString());
        //        _invoicetaxrate = (int)ReturnNumbersOnly(_settings["InvoiceTaxrate"].ToString());
        //    }
        //    catch
        //    {
        //        //nothing
        //    }
        //    _settings.Clear();

        //    //calculate contract duration
        //    if (startdate == null) startdate = DateTime.Now;
        //    if (_planvaliddays == 0) _planvaliddays = _Invoice_Valid_Days;
        //    DateTime _duedate = startdate.AddDays(_planvaliddays);
                        
        //    //get company id based on userid
        //    if (!String.IsNullOrEmpty(userid)) {
        //        _companyid = (from c in db.t_companies
        //                      where c.c_repuserid == userid
        //                      select c.c_id)
        //                      .FirstOrDefault();
        //    }
          
        //    //save invoice
        //    t_invoices _newinvoice = new t_invoices();
        //    _newinvoice.inv_refnumber = _invoiceref;
        //    _newinvoice.inv_planid = (int)planid;
        //    _newinvoice.inv_description = _planname + " plan, with steurpartner. Subscription period " + ReturnCustomDate(startdate, "") + " to " + ReturnCustomDate(_duedate, "");
        //    _newinvoice.inv_validdays = _planvaliddays;
        //    _newinvoice.inv_startdate = startdate;
        //    _newinvoice.inv_duedate = _duedate;
        //    //inv_paymentref
        //    _newinvoice.inv_status = _pendingstatus;
        //    _newinvoice.inv_creationtimestamp = DateTime.Now;
        //    _newinvoice.inv_userid = userid;
        //    if (_companyid != 0) { _newinvoice.inv_companyid = _companyid; }
        //    else { _newinvoice.inv_companyid = null; }
        //    _newinvoice.inv_billingcurrencycode = "EUR";
            
        //    if (_billedamount > 0)
        //    {
        //        //bill amount /100 * rate
        //        decimal _taxamount = (decimal)((decimal)_billedamount / 100 * (decimal)_invoicetaxrate);
        //        _billedamount = _billedamount + _taxamount;
        //        //_newinvoice.inv_taxbilled = _taxamount;
        //    }

        //    _newinvoice.inv_amountbilled = _billedamount;

        //    //inv_amountpaid = 
        //    //inv_datepaid = 
        //    _newinvoice.inv_lastupdate = DateTime.Now;
        //    _newinvoice.inv_lastupdateby = ReturnLastUpdateby(false, request);

        //    //save if no existing invoice with same ref
        //    var _foundidenticalinvoice = (from i in db.t_invoices
        //                                  where i.inv_planid == planid && i.inv_status == _pendingstatus && i.inv_refnumber == _invoiceref
        //                                  select i.inv_id)
        //                                  .FirstOrDefault();

        //    if (_foundidenticalinvoice == 0)
        //    {
        //        db.t_invoices.Add(_newinvoice);
        //        db.SaveChanges();
        //        //log entry
        //        string _username = "";

        //        try
        //        {
        //            _username = (from u in db.AspNetUsers where u.Id == userid select u.UserName).FirstOrDefault();
        //        }
        //        catch { }

        //        var _logentry = "New Invoice for " + _username;
        //        RecordLog(_logentry, "New Invoice", request, 1);
        //        _return = "success";
        //    }
        //    return _return;
        //}

        //public static void ActivateUserAccount(string UserId, string CurrentRole, string NewRole, HttpRequestBase request)
        //{
        //    //Activate user account
        //    var _store = new UserStore<ApplicationUser>(new ApplicationDbContext());
        //    var _userManager = new UserManager<ApplicationUser>(_store);
        //    var _useraccount = _userManager.FindById(UserId);
        //    _useraccount.AccessFailedCount = 0;
        //    _useraccount.LockoutEndDateUtc = null; //DateTime.Now.AddSeconds(-5);
        //    _useraccount.EmailConfirmed = true;
        //    _useraccount.PhoneNumberConfirmed = true;

        //    //change user role
        //    var userRoles = _userManager.GetRoles(_useraccount.Id);
        //    _userManager.AddToRole(_useraccount.Id, NewRole);
        //    _userManager.RemoveFromRole(_useraccount.Id, CurrentRole);

        //    //send confirmation email to user
        //    try
        //    {
        //        string _emailsubject = "Your steurpartner account has been Activated";
        //        string _emailbody = "Your steurpartner account has been Activated";

        //        //SendMail(_useraccount.Email.ToString(), "", _emailsubject, _emailbody, true);
        //    }
        //    catch { }

        //    //log new account activation
        //    RecordLog("Account Activated", "AL-Paid", request, 1);
        //}
        //public static string ReturnBannerStatus(DateTime? enddate)
        //{
        //    string _return = "active";

        //    try
        //    {
        //        int _status = DateTime.Compare(DateTime.Now, enddate ?? DateTime.Now);
        //        if (_status == 1) _return = "expired"; //you can add more status....
        //    }
        //    catch { }

        //    return _return;
        //}
        //public static string CheckAliasAvailability(string _alias, int company_id) {
        //    TP_DataEntities db = new TP_DataEntities();
        //    string _return = "";

        //    try
        //    {
        //        var _resultset = (from c in db.t_companies
        //                          where c.c_systemurl == _alias && c.c_id != company_id
        //                          select new { c.c_id, c.c_systemurl }).FirstOrDefault();
        //        //where c.c_systemurl == _alias && c.c_id!= (from c2 in db.t_companies where c2.c_systemurl==_alias select c2.c_id).FirstOrDefault()

        //        if (_resultset != null)
        //        {
        //            //check if alias belongs to the selected company
        //            //if(_resultset.)
        //            _return = "nok";
        //        }
        //        else
        //        {
        //            _return = "ok";
        //        }
        //    }
        //    catch { }

        //    return _return;
        //}

        //public static string CheckUserAvailability(string _proposedemail, string _proposedusername)
        //{
        //    TP_DataEntities db = new TP_DataEntities();
        //    string _return = "nok";

        //    try
        //    {

        //        //check email
        //        if (!String.IsNullOrEmpty(_proposedemail))
        //        {
        //           var  _resultset = (from r in db.AspNetUsers
        //                          where r.Email == _proposedemail
        //                          select new { r.Id }).FirstOrDefault();
        //            if (_resultset == null) _return = "ok";
        //            return _return;
        //        }

        //        if (!String.IsNullOrEmpty(_proposedusername))
        //        {
        //            //check username
        //           var _resultset = (from r in db.AspNetUsers
        //                              where r.UserName == _proposedusername
        //                              select new { r.Id }).FirstOrDefault();
        //            if (_resultset == null) _return = "ok";
        //            return _return;
        //        }
        //    }
        //    catch { }

        //    return _return;
        //}

        // hashing: Md5
        public static string GetMd5Sum(string str)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(str);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
                // To force the hex string to lower-case letters instead of
                // upper-case, use he following line instead:
                // sb.Append(hashBytes[i].ToString("x2")); 
            }
            return sb.ToString();

        }

        //hashing: SHA1
        public static string GetSHA1(string s_string)
        {
            string s_return = "";

            SHA1 sha = new SHA1Managed();
            ASCIIEncoding ae = new ASCIIEncoding();
            byte[] data = ae.GetBytes(s_string);
            byte[] digest = sha.ComputeHash(data);

            //return as hex
            StringBuilder s = new StringBuilder();
            int length = digest.Length;
            for (int n = 0; n < length; n++)
            {
                s.Append(String.Format("{0,2:X}", digest[n]).Replace(" ", "0"));
            }

            s_return = s.ToString();

            return s_return;

        }

        //encode BASE64
        //decode BASE64
        public static string ReturnEcodedString(string s_encodingmethod, string s_input)
        {
            string s_return = "";

            switch (s_encodingmethod.ToLower())
            {
                case "base64":
                    //decode base 64 string
                    var _bytes = Encoding.UTF8.GetBytes(s_input);
                    s_return = Convert.ToBase64String(_bytes);

                    break;
            }

            return s_return;
        }

        //decode BASE64
        public static string ReturnDecodedString(string s_encodingmethod, string s_input)
        {
            string s_return = "";

            switch (s_encodingmethod.ToLower())
            {
                case "base64":
                    //decode base 64 string
                    byte[] data = Convert.FromBase64String(s_input);
                    s_return = Encoding.UTF8.GetString(data);
                    break;
            }

            return s_return;
        }

        //post data and return response as a string
        public static string PostData(string s_url, string s_postmethod, string s_parameters,
            string s_actionheader = "", string s_contenttype = "application/x-www-form-urlencoded", X509Certificate2 X509_cert = null)
        {

            string s_return = "";
            string strPost = s_parameters;

            //check if url is blank
            if (s_url == "" || s_url == null) { return s_return; }

            Stream strm_writer = null;
            HttpWebRequest h_WebReq = (HttpWebRequest)WebRequest.Create(s_url);

            ////Add Certificate if any
            //if (X509_cert != null)
            //    h_WebReq.ClientCertificates.Add(X509_cert);

            h_WebReq.Timeout = 120000;  //120 seconds
            h_WebReq.ServicePoint.MaxIdleTime = 120000;  //120 seconds

            byte[] requestBytes = System.Text.Encoding.ASCII.GetBytes(s_parameters);
            h_WebReq.Method = s_postmethod;

            h_WebReq.ContentType = s_contenttype;       //some webservices are sensitive to this
            //h_WebReq.Accept = "";     //if need be

            //Add Header if necessary
            if (s_actionheader != "")
                h_WebReq.Headers.Add("SOAPAction: " + s_actionheader);

            //Add Certificate if any
            if (X509_cert != null)
            {
                h_WebReq.ClientCertificates.Add(X509_cert);

                //System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => { return true; };
                SSLValidator.OverrideValidation();
            }

            h_WebReq.ContentLength = requestBytes.Length;

            try
            {
                strm_writer = h_WebReq.GetRequestStream();
                strm_writer.Write(requestBytes, 0, requestBytes.Length);
                strm_writer.Close();
            }
            catch (Exception ex)
            {
                //Log Error:: General Error Posting Parameters
                LogMessageToFile("GSE_P :: General Error Posting Parameters - " + ex.ToString() + " [" + s_url + s_parameters + "]", "error");

                ////Send Email Notification to Developer
                //TP_DataEntities db = new TP_DataEntities();
                //var _settings = db.t_systemsettings.ToDictionary(key => key.sys_param, val => val.sys_value);

                //string s_EMAIL_ADDRESS_ERROR_NOTIFICATION = "";
                //try
                //{
                //    s_EMAIL_ADDRESS_ERROR_NOTIFICATION = _settings["EMAIL_ADDRESS_ERROR_NOTIFICATION"].ToString();
                //}
                //catch
                //{
                //    //nothing to do here
                //}

                ////Clear Settings Table
                //_settings.Clear();

                //SendMail(s_EMAIL_ADDRESS_ERROR_NOTIFICATION, s_EMAIL_ADDRESS_ERROR_NOTIFICATION,
                //    "Payment Page GSE_P",
                //    "GSE_P :: General Error Posting Parameters - " + ex.ToString() + " [" + s_url + s_parameters + "]", true);
            }
            finally
            {
                //strm_writer.Close();
            }
            //process response
            try
            {
                HttpWebResponse h_WebResponse = (HttpWebResponse)h_WebReq.GetResponse();

                using (StreamReader sr =
                    new StreamReader(h_WebResponse.GetResponseStream()))
                {
                    s_return = sr.ReadToEnd();
                    // Close and clean up the StreamReader
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                //Log Error:: General Error Posting Parameters
                Common.LogMessageToFile("GSE_R :: General Error Receiving Parameters - " + ex.ToString() + " [" + s_url + s_parameters + "]", "error");

                ////Send Email Notification to Developer
                //TP_DataEntities db = new TP_DataEntities();
                //var _settings = db.t_systemsettings.ToDictionary(key => key.sys_param, val => val.sys_value);

                //string s_EMAIL_ADDRESS_ERROR_NOTIFICATION = "";
                //try
                //{
                //    s_EMAIL_ADDRESS_ERROR_NOTIFICATION = _settings["EMAIL_ADDRESS_ERROR_NOTIFICATION"].ToString();
                //}
                //catch
                //{
                //    //nothing to do here
                //}

                ////Clear Settings Table
                //_settings.Clear();

                //SendMail(s_EMAIL_ADDRESS_ERROR_NOTIFICATION, s_EMAIL_ADDRESS_ERROR_NOTIFICATION,
                //    "Payment Page GSE_R",
                //    "GSE_R :: General Error Receiving Parameters - " + ex.ToString() + " [" + s_url + s_parameters + "]", true);

                s_return = "";
            }

            return s_return;
        }

        //Get Certificate by Serial Number
        public static X509Certificate2 GetCertificatebySerial(string s_serialnumber, string s_store = "")
        {
            //default store name
            StoreName storename = StoreName.Root;

            switch (s_store.ToLower())
            {
                case "addressbook":
                    storename = StoreName.AddressBook;
                    break;
                case "authroot":
                    storename = StoreName.AuthRoot;
                    break;
                case "certificateauthority":
                    storename = StoreName.CertificateAuthority;
                    break;
                case "disallowed":
                    storename = StoreName.Disallowed;
                    break;
                case "my":
                    storename = StoreName.My;
                    break;
                case "trustedpeople":
                    storename = StoreName.TrustedPeople;
                    break;
                case "trustedpublisher":
                    storename = StoreName.TrustedPublisher;
                    break;
            }

            X509Store store = new X509Store(storename, StoreLocation.LocalMachine);
            try
            {
                store.Open(OpenFlags.ReadOnly);

                X509Certificate2Collection collection = store.Certificates.Find(X509FindType.FindBySerialNumber, s_serialnumber, true);

                return collection[0];
            }

            catch (Exception pex)
            {
                //log error:: certificate not found
                LogMessageToFile("certificate error [" + s_store + " - " + s_serialnumber + "] - " + pex.ToString(), "error");
            }

            finally { store.Close(); }

            //log error:: certificate not found
            LogMessageToFile("certificate not found [" + s_store + " - " + s_serialnumber + "]", "error");
            return null;
        }

        //Log Files
        public static string GetPath()
        {
            string path = HttpContext.Current.Server.MapPath("/Content/AppLogs");

            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }
            return path;
        }
        //Log Files
        public static void LogMessageToFile(string message, string s_filetype)
        {
            //yyyy,mm,dd
            string s_filename = "unknowntypes.txt";

            switch (s_filetype.ToLower())
            {
                case "error":
                    s_filename = "Err" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".txt";
                    break;
                case "info":
                    s_filename = "Info" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".txt";
                    break;
                case "mailerror":
                    s_filename = "MailErr" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".txt";
                    break;
                case "test":
                    s_filename = "test" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".txt";
                    break;
            }

            try
            {
                StreamWriter sw = File.AppendText(GetPath() + s_filename);
                string logLine = String.Format("{0:G}: {1}.", DateTime.Now, message);
                sw.WriteLine(logLine);
                sw.Close();
            }
            catch
            {
                //nothing to record here
            }
        }

        //Return Error Message from xml file :: search using error code
        public static string ReturnError(string s_errorcode, string s_returnfield)
        {
            string s_return = "";

            try
            {
                var requestcontext = HttpContext.Current.Request.RequestContext;
                var errordb = new System.Web.Mvc.UrlHelper(requestcontext).Action("ErrorsDB", "ErrorMessages", new { area = "PaySecAdmin" }, HttpContext.Current.Request.Url.Scheme);

                XDocument doc = XDocument.Load(errordb);

                XElement element = doc.Element("ErrorMessages")
                    .Elements("cError").Elements("ErrorCode")
                    .SingleOrDefault(x => x.Value == s_errorcode.ToString());

                if (element != null)
                {
                    XElement parent = element.Parent;
                    s_return = parent.Element(s_returnfield).Value.ToString();
                }
            }
            catch (Exception ex)
            {
                LogMessageToFile("Common:ReturnError |ErrorCode: " + s_errorcode + " ReturnField: " + s_returnfield + "|" + ex.ToString(), "error");
            }

            return s_return;
        }

        //Return Error Message from xml file :: search using error code
        public static string ReturnExternalErrorMeaning(string s_errorcode, string s_returnfield)
        {
            string s_return = "";

            try
            {
                var requestcontext = HttpContext.Current.Request.RequestContext;
                var errordb = new System.Web.Mvc.UrlHelper(requestcontext).Action("ErrorsDB_ExternalProviders", "ErrorMessages", new { area = "PaySecAdmin" }, HttpContext.Current.Request.Url.Scheme);

                XDocument doc = XDocument.Load(errordb);

                XElement element = doc.Element("ExternalCodes")
                    .Elements("ErrorCode").Elements("Code")
                    .SingleOrDefault(x => x.Value == s_errorcode.ToString());

                if (element != null)
                {
                    XElement parent = element.Parent;
                    s_return = parent.Element(s_returnfield).Value.ToString();
                }
            }
            catch (Exception ex)
            {
                LogMessageToFile(ex.ToString(), "error");
            }
            return s_return;
        }

        #region string functions
        //sanitized it using accepted values
        //private static AjaxControlToolkit.Sanitizer.HtmlAgilityPackSanitizerProvider sanitizer = new AjaxControlToolkit.Sanitizer.HtmlAgilityPackSanitizerProvider();
        //private static sanitizer = HtmlSanitize
        //HtmlParser
        public static string CleanWithAcceptedValues(string s_toclean, string[] sar_exceptionalvalues = null, string s_whitelistdefault = "([^A-Za-z0-9@.,äüöß:'&.' _-]+)")
        {
            string s_return = s_toclean;
            //whitelist: ([^A-Za-z0-9@.'&.' _-]+)

            try
            {
                //WhiteList :: A-Z, a-z, 0-9, ‘@, &’ sign, a period
                System.Text.RegularExpressions.Regex _regxwhitelist = new System.Text.RegularExpressions.Regex(s_whitelistdefault);     //new System.Text.RegularExpressions.Regex("([^A-Za-z0-9@.'&.' _-]+)");
                s_return = _regxwhitelist.Replace(s_toclean, "");
            }
            catch { }

            return s_return;
        }

        //Convert String to SentenceCase
        public static string StrToSentenceCase(string s_string)
        {

            return System.Text.RegularExpressions.Regex.Replace(s_string, @"\b[a-z]\w+", delegate (System.Text.RegularExpressions.Match match)
            {
                string v = match.ToString();
                return char.ToUpper(v[0]) + v.Substring(1);
            });
        }

        //return SomeText.Substring(0, (SomeText.Length > 64 ? 64 : SomeText.Length)).Replace(" ", "-").Replace("#", "-");
        //return Regex.Replace(SomeText.Substring(0, (SomeText.Length > 64 ? 64 : SomeText.Length)).Replace("'", ""), @"[^a-zA-Z0-9]", "-").Replace("--", "-");
        public static string StrRight(string s_original, int i_numberofcharacters)
        {
            string s_return = "";
            try { s_return = s_original.Substring(s_original.Length - i_numberofcharacters); }
            catch { }
            return s_return;
        }

        //sanitize and return numbers
        public static decimal ReturnNumbersOnly(string s_toclean)
        {
            decimal d_return = 0;
            string s_computed = "";
            try
            {
                for (int i = 0; i < s_toclean.Length; i++)
                {
                    if (IsNumeric(s_toclean[i].ToString()))
                        s_computed += s_toclean[i].ToString();
                }
            }
            catch
            {
                d_return = 0;
            }

            if (s_computed != "")
                d_return = Convert.ToDecimal(s_computed);

            return d_return;
        }

        //check for numbers
        public static bool IsNumeric(string s_Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(s_Expression, System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static string ReturnDateTimeSingleString()
        {
            return Regex.Replace(DateTime.Now.ToString(), @"[^0-9]", "");
        }

        //return custom date format
        public static string ReturnCustomDate(DateTime? _datetoformat, string _returnformat)
        {
            //if (String.IsNullOrEmpty(_returnformat)) _returnformat = "{0:yyyy.MMM.dd}";
            //default return
            if (String.IsNullOrEmpty(_returnformat))
                _returnformat = "dd.mmm.yyyy";       //_returnformat = "{0:dd.MMM.yyy}";

            string _return = "";
            try
            {

                //_return = String.Format(_returnformat, _datetoformat);
                var _day = _datetoformat.Value.Day.ToString();
                var _year = _datetoformat.Value.Year.ToString();
                var _monthname = ReturnMonthName(_datetoformat.Value.Month);
                var _month = _datetoformat.Value.Month.ToString();

                switch (_returnformat)
                {
                    case "dd.mmm.yyyy":
                        //8. Okt. 2015 
                        _return = _day + ". " + _monthname + " " + _year;
                        break;
                    case "dd/MM/yyyy":
                        if (_day.Length == 1) _day = "0" + _day;
                        if (_month.Length == 1) _month = "0" + _month;
                        _return = _day + "/" + _month + "/" + _year;
                        break;
                }

            }
            catch
            {
                _return = String.Format("{0:dd.MMM.yyy}", _datetoformat);
            }

            return _return;
        }
        public static string ReturnMonthName(int Month, string returntype = "")
        {
            string _return = "";
            switch (Month)
            {
                case 1:
                    _return = "Jän.";
                    break;
                case 2:
                    _return = "Feb.";
                    break;
                case 3:
                    _return = "Mär.";
                    break;
                case 4:
                    _return = "Apr.";
                    break;
                case 5:
                    _return = "Mai";
                    break;
                case 6:
                    _return = "Jun.";
                    break;
                case 7:
                    _return = "Jul.";
                    break;
                case 8:
                    _return = "Aug.";
                    break;
                case 9:
                    _return = "Sep.";
                    break;
                case 10:
                    _return = "Okt.";
                    break;
                case 11:
                    _return = "Nov.";
                    break;
                case 12:
                    _return = "Dez.";
                    break;
            }
            //switch (returntype.ToLower()) {
            //    case "":
            //        break;
            //}
            return _return;
        }
        #endregion
        //public static byte[] huarunwei()
        //{

        //    string inputFn = @"c:\temp\test.txt";
        //    MemoryStream ms = new MemoryStream();
        //    ZipOutputStream zs = new ZipOutputStream(ms);
        //    zs.SetLevel(3); //0-9, 9 being the highest level of compression
        //    //zs.Password = password; // optional

        //    ZipEntry newEntry = new ZipEntry(Path.GetFileName(inputFn)); // Call it what you will here
        //    newEntry.DateTime = DateTime.Now;
        //    zs.UseZip64 = UseZip64.Off;
        //    zs.PutNextEntry(newEntry);

        //    FileStream fs = File.OpenRead(inputFn);
        //    byte[] buffer = new byte[4096]; // optimum size

        //    int sourceBytes;
        //    do
        //    {
        //        sourceBytes = fs.Read(buffer, 0, buffer.Length);
        //        zs.Write(buffer, 0, sourceBytes);
        //    } while (sourceBytes > 0);

        //    fs.Close();
        //    zs.CloseEntry();
        //    zs.IsStreamOwner = false;    // True makes the Close also Close the underlying stream
        //    zs.Close();
        //    byte[] zipByteArray = ms.ToArray();
        //    return zipByteArray;
        //}
        //public static void SendMail(string s_ToAddress, string s_ToName, string s_Subject, string s_Body, bool b_ishtml)
        //{

        //    //read from settings table
        //    //TP_DataEntities db = new TP_DataEntities();
        //    DbManager db = new DbManager();
        //    //var _settings1 = db.SsettingViewModels.To
        //    var _settings = db.SsettingViewModels.ToDictionary(key => key.Sysparam, val => val.Sysvalue);

        //    string s_C_MailFromAddress = "";
        //    string s_C_MailFromName = "";
        //    string s_C_MailServer = "";
        //    string s_C_MailServerCredentialUserName = "";
        //    string s_C_MailServerCredentialPassword = "";
        //    string s_EMAIL_SENDER_SSLENABLED = "";
        //    int s_C_MailServerPort = 0;

        //    try
        //    {
        //        s_C_MailFromAddress = _settings["EMAIL_ADDRESS_FROM"].ToString();
        //        s_C_MailFromName = _settings["EMAIL_SENDER_NAME"].ToString();
        //        s_C_MailServer = _settings["EMAIL_SERVER"].ToString();
        //        s_C_MailServerPort = (int)Common.ReturnNumbersOnly(_settings["EMAIL_SERVERPORT"].ToString());
        //        s_C_MailServerCredentialUserName = _settings["EMAIL_USERNAME"].ToString();
        //        s_C_MailServerCredentialPassword = _settings["EMAIL_PWD"].ToString();
        //        s_EMAIL_SENDER_SSLENABLED = _settings["EMAIL_SENDER_SSLENABLED"].ToString();
        //    }
        //    catch
        //    {
        //        //nothing to do here
        //    }

        //    //b_ishtml = true; //set default mail format if necessary
        //    SmtpClient smtpClient = new SmtpClient();
        //    MailMessage objMail = new MailMessage();

        //    MailAddress objMail_fromaddress = new MailAddress(s_C_MailFromAddress, s_C_MailFromName);
        //    MailAddress objMail_toaddress = new MailAddress(s_ToAddress, s_ToName);

        //    objMail.From = objMail_fromaddress;
        //    objMail.To.Add(objMail_toaddress);

        //    if (b_ishtml == true)
        //    {
        //        //html format
        //        objMail.IsBodyHtml = true;
        //    }
        //    else
        //    {
        //        //text format
        //        objMail.IsBodyHtml = false;
        //    }
        //    objMail.Subject = s_Subject;
        //    objMail.Body = s_Body;

        //    //objMail.Priority = MailPriority.Normal;
        //    smtpClient.Host = s_C_MailServer;
        //    if (s_C_MailServerPort != 0) smtpClient.Port = s_C_MailServerPort;

        //    smtpClient.Credentials = new System.Net.NetworkCredential(s_C_MailServerCredentialUserName, s_C_MailServerCredentialPassword);
        //    if (s_EMAIL_SENDER_SSLENABLED == "true")
        //        smtpClient.EnableSsl = true;
        //    try
        //    {
        //        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
        //        smtpClient.Send(objMail);
        //    }
        //    catch (Exception ee)
        //    {
        //        //write to log file.
        //        LogMessageToFile("Sending Server (" + s_C_MailServer + ") | To (" + s_ToAddress + "): " + ee.ToString(), "mailerror");
        //    }
        //}

        public static class SSLValidator
        {
            private static bool OnValidateCertificate(object sender, X509Certificate certificate, X509Chain chain,
                                                      SslPolicyErrors sslPolicyErrors)
            {
                return true;
            }

            public static void OverrideValidation()
            {
                ServicePointManager.ServerCertificateValidationCallback = OnValidateCertificate;
                ServicePointManager.Expect100Continue = true;
            }
        }

        //depreciated
        public class TrustAllCertificatePolicy : System.Net.ICertificatePolicy
        {
            public TrustAllCertificatePolicy()
            { }
            public bool CheckValidationResult(ServicePoint sp,
               System.Security.Cryptography.X509Certificates.
                X509Certificate cert, WebRequest req, int problem)
            {

                return true;
            }
        }

        public class Constants
        {
            //*** THESE KEYS HAS BEEN INCLUDED IN THE CODE FOR SECURITY REASONS
            //public const string s_OGONE_C_KEY = "The*sun*shines*@eva1INDEV"; //*** Ogone Secret Key for SHA_IN
            //public const string s_OGONE_C_KEY_OUT = "The*sun*shines*@eva1OUTDEV"; //*** Ogone Secret Key for SHA_OUT
            //prod
            public const string s_OGONE_C_KEY = "PRDSunshine.2014"; //*** Ogone Secret Key for SHA_IN
            public const string s_OGONE_C_KEY_OUT = "PRDSunshine.2014"; //*** Ogone Secret Key for SHA_OUT

            public const string s_OGONE_C_APIUSER = "APIUser1"; //*** Ogone API Username
            public const string s_OGONE_C_APIKEY = ""; //*** Ogone API Password

            public enum InvoiceStatus { Paid, Pending, Due, Overdue, PaymentProcessing };
            public enum PredefinedCompanyDataFields { Email, Website, Phone, Mobile, Fax };

            public enum BannerSections
            {
                Homepage_BlogSection, Homepage_CompanyNewsSection_1, Homepage_CompanyNewsSection_2, Homepage_PressReleaseSection, Homepage_JobSearchSection,
                Blogpage_TaxBlogListingsPage, Blogpage_3rdpartyBlogListingsPage, Blogpage_BlogPost, Blogpage_Annoucement,
                AccountHolder_Dashboardpage, AccountHolder_Subscriptionpage, AccountHolder_Invoicespage, AccountHolder_Corporatedatapage,
                SearchResults_1, SearchResults_2, SearchResults_3
            }

        }
    }
}