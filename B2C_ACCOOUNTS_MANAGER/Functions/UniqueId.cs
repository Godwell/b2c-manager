﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B2C_ACCOOUNTS_MANAGER.Functions
{
    public class UniqueId
    {
        public UniqueId()
        {
        }

        public Guid GetId()
        {
            return Guid.Parse(Guid.NewGuid().ToString("N"));
        }
    }
}