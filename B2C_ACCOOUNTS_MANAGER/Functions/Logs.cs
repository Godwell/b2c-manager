﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace B2C_ACCOOUNTS_MANAGER.Functions
{
    public class Logs
    {
        public void LogError(string username, string form, string description, string clientIp, string fPath)
        {
            //userName = username;
            //fnErrorLog(description, form, fPath);
            //var model = new ERRORLOG();
            //model.Id = _uniqueId.GetId();
            //model.Osysdate = DateTime.Now;
            //model.Form = form;
            //model.ClientIp = clientIp;
            //model.Description = description;
            //model.Username = username;
            //_repository.SaveNew(model);
        }

        public void fnErrorLog(string error, string frm, string fPath)
        {
            try
            {
                string usr = HttpContext.Current.User.Identity.GetUserName();
            
                string errorLog = fPath + @"\Errors\" + string.Format("{0:yyyyMMdd}", DateTime.Now) + @".log";
                if (!Directory.Exists(Path.GetDirectoryName(errorLog)))
                    Directory.CreateDirectory(Path.GetDirectoryName(errorLog));

                //Get the file for exclusive read/write access
                FileStream fStream = new FileStream(errorLog, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                StreamWriter sWriter = new StreamWriter(fStream);
                sWriter.Close();
                fStream.Close();

                //Error Logging Starts
                FileStream fStreamDup = new FileStream(errorLog, FileMode.Append, FileAccess.Write);
                StreamWriter sWrite = new StreamWriter(fStreamDup);
                sWrite.WriteLine("Error: " + error + " occured on: " + frm + ", at: " + DateTime.Now.ToString() + " User: " + usr);
                //sWriterDup.WriteLine();
                sWrite.Close();
                fStreamDup.Close();
            }
            catch //(Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
    }
}