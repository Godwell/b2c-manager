﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B2C_ACCOOUNTS_MANAGER.Functions
{
    public class MyEnums
    {
        //public enum StatusOptions
        //{
        //    Active = 1,
        //    Deactive = 0,
        //}
        public enum StatusOption
        {
            Added = 1,
            Approved = 2,
            Deleted = 3,
            Unapproved = 4,
            Rejected = 5,
            Edited = 6,
            PendingDeletion = 7,
            Pendinglock = 8,
            Locked = 9,

        }
    }
}