namespace B2C_ACCOOUNTS_MANAGER.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using B2C_ACCOOUNTS_MANAGER.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using B2C_ACCOOUNTS_MANAGER.Models.UOW;
    using B2C_ACCOOUNTS_MANAGER.Functions;

    internal sealed class Configuration : DbMigrationsConfiguration<B2C_ACCOOUNTS_MANAGER.Models.ApplicationDbContext>
    {
        private readonly Repository _repository;
        private readonly UniqueId _uniqueId = new UniqueId();
        private ApplicationUserManager _userManager;
        B2C_MANAGEREntities db = new B2C_MANAGEREntities();
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(B2C_ACCOOUNTS_MANAGER.Models.ApplicationDbContext context)
        {
            var roles = new[] { "SuperAdmin", "SaccoAdmin" };
            var rolesStore = new RoleStore<IdentityRole>(context);
            var rolemanager = new RoleManager<IdentityRole>(rolesStore);
            foreach (var n in roles)
            {
                if (!context.Roles.Any(p => p.Name == n))
                {
                    rolemanager.Create(new IdentityRole { Name = n });
                }
            }
            var users = new[] { "admin1", "admin2" };
            foreach (var n in users)
            {
                var y = context.Users.Any(p => p.UserName == n.ToLower());
                if (y == false)
                {
                    var store = new UserStore<ApplicationUser>(context);
                    var userManager = new UserManager<ApplicationUser>(store);
                    var user = new ApplicationUser
                    {
                        UserName = n.ToLower(),
                        Email = "godwellm@gmail.com",
                        PhoneNumber = "0719119560",
                        EmailConfirmed = true,
                        org_code = 1,
                        PhoneNumberConfirmed = true,
                        TwoFactorEnabled = true,
                        LockoutEnabled = true,
                        AccessFailedCount = 0,
                        //statuse=true
                    };
                    userManager.Create(user, user.UserName.ToLower() + 1234);

                    var selectedrole = db.AspNetRoles.OrderByDescending(p => p.Name).FirstOrDefault();
                    //.Where(p => p.Name == "SuperAdmin");
                    var _resultuserrole = userManager.AddToRole(user.Id, selectedrole.Id);
                    if (!_resultuserrole.Succeeded)
                    {
                        //return View();
                    }
                }

            }
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
