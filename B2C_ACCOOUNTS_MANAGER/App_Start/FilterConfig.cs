﻿using System.Web;
using System.Web.Mvc;

namespace B2C_ACCOOUNTS_MANAGER
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
