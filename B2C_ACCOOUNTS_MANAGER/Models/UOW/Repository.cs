﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace B2C_ACCOOUNTS_MANAGER.Models.UOW
{
    public class Repository : IRepository
    {
        private readonly ApplicationDbContext _db;

        /// <summary>
        /// Initializes a new instance of the DataRepository class
        /// </summary>
        public Repository()
        {
            _db = new ApplicationDbContext();
        }

        /// <summary>
        /// Fetches the entire class set as an IQueryable
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <returns>An IQueryable of the class</returns>
        public IQueryable<T> Fetch<T>() where T : class
        {
            return _db.Set<T>().AsQueryable();
        }



        /// <summary>
        /// Used to query a result set from the class set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing all the filter criteria</param>
        /// <returns>An IQueryable of the class</returns>
        public IQueryable<T> Search<T>(Func<T, bool> predicate) where T : class
        {
            return _db.Set<T>().Where(predicate).AsQueryable();
        }

        /// <summary>
        /// Returns the single object matching the criteria specified
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing all the filter criteria</param>
        /// <returns>A single oject of the class</returns>
        public T Single<T>(Func<T, bool> predicate) where T : class
        {
            return _db.Set<T>().SingleOrDefault(predicate);
        }

        /// <summary>
        /// Returns the first oject matching the criteria specified
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing all the filter criteria</param>
        /// <returns>The first oject of the class matching the criteria</returns>
        public T First<T>(Func<T, bool> predicate) where T : class
        {
            return _db.Set<T>().FirstOrDefault(predicate);
        }

        public T Find<T>(Guid id) where T : class
        {
            return _db.Set<T>().Find(id);
        }



        /// <summary>
        /// Saves all changes to the database
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="entity">The object to be saved to the database</param>
        public void SaveNew<T>(T entity) where T : class
        {
            _db.Set<T>().Add(entity);
            SaveChanges();
        }


        /// <summary>
        /// Saves all edited objects to the database
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="entity">The object to be saved to the database</param>
        public void SaveUpdate<T>(T entity) where T : class
        {
            _db.Set<T>().Attach(entity);
            _db.Entry(entity).State = EntityState.Modified;
            SaveChanges();
        }

        /// <summary>
        /// Gets the maximum object from the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing criteria field</param>
        /// <returns>The maximum oject of the class matching the criteria</returns>
        public T Max<T>(Func<T, bool> predicate) where T : class
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the minimum object from the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing criteria field</param>
        /// <returns>The minimum oject of the class matching the criteria</returns>
        public T Min<T>(Func<T, bool> predicate) where T : class
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if any object matching exists in the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing criteria field(s)</param>
        /// <returns>True if an object is found, false otherwise</returns>
        public bool Exists<T>(Func<T, bool> predicate) where T : class
        {
            return _db.Set<T>().Any(predicate);
        }

        /// <summary>
        /// Saves multiple new objects in the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="entities">A list of objects to be saved</param>
        public void SaveNew<T>(List<T> entities) where T : class
        {
            foreach (T entity in entities)
                _db.Set<T>().Add(entity);
            SaveChanges();
        }

        /// <summary>
        /// Saves multiple edited objects in the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="entities">A list of objects to be saved</param>
        public void SaveUpdate<T>(List<T> entities) where T : class
        {
            foreach (T entity in entities)
                _db.Set<T>().Attach(entity);
            SaveChanges();
        }

        /// <summary>
        /// Deleted an object from the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="entity">The object to be deleted to the database</param>
        public void Delete<T>(T entity) where T : class
        {
            _db.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Deletes all objects that match the criteria from the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing criteria field(s)</param>
        public void Delete<T>(Func<T, bool> predicate) where T : class
        {
            foreach (T entity in Search<T>(predicate))
                _db.Set<T>().Remove(entity);
            SaveChanges();
        }

        /// <summary>
        /// Saves all changes to the database
        /// </summary>
        public void SaveChanges()
        {
            _db.SaveChanges();
        }

        /// <summary>
        /// Saves all changes to the database based on the specified options
        /// </summary>
        /// <param name="options">The database save options to be applied</param>
        public void SaveChanges(SaveOptions options)
        {
            _db.SaveChanges();
        }




        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases all resources used by the WarrantManagement.DataExtract.Dal.ReportDataBase
        /// </summary>
        /// <param name="disposing">A boolean value indicating whether or not to dispose managed resources</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_db != null)
                    _db.Dispose();
            }
        }
    }
}