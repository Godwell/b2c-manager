﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B2C_ACCOOUNTS_MANAGER.Models.UOW
{
    public interface IRepository : IDisposable
    {
        /// <summary>
        /// Fetches the entire class set as an IQueryable
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <returns>An IQueryable of the class</returns>
        IQueryable<T> Fetch<T>() where T : class;


        /// <summary>
        /// Used to query a result set from the class set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing all the filter criteria</param>
        /// <returns>An IQueryable of the class</returns>
        IQueryable<T> Search<T>(Func<T, bool> predicate) where T : class;

        /// <summary>
        /// Returns the single object matching the criteria specified
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing all the filter criteria</param>
        /// <returns>A single oject of the class</returns>
        T Single<T>(Func<T, bool> predicate) where T : class;

        /// <summary>
        /// Returns the first oject matching the criteria specified
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing all the filter criteria</param>
        /// <returns>The first oject of the class matching the criteria</returns>
        T First<T>(Func<T, bool> predicate) where T : class;

        T Find<T>(Guid id) where T : class;


        /// <summary>
        /// Saves a new objects to the database
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="entity">The object to be saved to the database</param>
        void SaveNew<T>(T entity) where T : class;

        /// <summary>
        /// Saves an edited object to the database
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="entity">The object to be saved to the database</param>
        void SaveUpdate<T>(T entity) where T : class;

        /// <summary>
        /// Gets the maximum object from the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing criteria field</param>
        /// <returns>The maximum oject of the class matching the criteria</returns>
        T Max<T>(Func<T, bool> predicate) where T : class;

        /// <summary>
        /// Gets the minimum object from the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing criteria field</param>
        /// <returns>The minimum oject of the class matching the criteria</returns>
        T Min<T>(Func<T, bool> predicate) where T : class;

        /// <summary>
        /// Checks if any object matching exists in the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing criteria field(s)</param>
        /// <returns>True if an object is found, false otherwise</returns>
        bool Exists<T>(Func<T, bool> predicate) where T : class;

        /// <summary>
        /// Saves multiple new objects in the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="entities">A list of objects to be saved</param>
        void SaveNew<T>(List<T> entities) where T : class;

        /// <summary>
        /// Saves multiple edited objects in the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="entities">A list of objects to be saved</param>
        void SaveUpdate<T>(List<T> entities) where T : class;

        /// <summary>
        /// Deleted an object from the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="entity">The object to be deleted to the database</param>
        void Delete<T>(T entity) where T : class;

        /// <summary>
        /// Deletes all objects that match the criteria from the set
        /// </summary>
        /// <typeparam name="T">The class type in context</typeparam>
        /// <param name="predicate">A predicate construct containing criteria field(s)</param>
        void Delete<T>(Func<T, bool> predicate) where T : class;

        /// <summary>
        /// Saves all changes to the database
        /// </summary>
        void SaveChanges();

        /// <summary>
        /// Saves all changes to the database based on the specified options
        /// </summary>
        /// <param name="options">The database save options to be applied</param>
        void SaveChanges(SaveOptions options);


    }
}
