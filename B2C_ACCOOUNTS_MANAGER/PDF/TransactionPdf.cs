﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;
//using AuditLettersApp.Functions;
//using AuditLettersApp.Models;
//using AuditLettersApp.Models.Implementation;
//using AuditLettersApp.Models.PartialModels;
//using AuditLettersApp.Models.Shared;
using B2C_ACCOOUNTS_MANAGER.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Font = iTextSharp.text.Font;
using Image = iTextSharp.text.Image;
using B2C_ACCOOUNTS_MANAGER.Functions;

namespace B2C_ACCOOUNTS_MANAGER.PDF
{
    public class PdfHeaderFooter1 : PdfPageEventHelper
    {
        Logs _logs = new Logs();

        public iTextSharp.text.Image ImageHeader { get; set; }
        public iTextSharp.text.Image ImageFooter { get; set; }
        // write on top of document
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            string codes = HttpContext.Current.Session["orgcode"].ToString();

            B2C_MANAGEREntities db = new B2C_MANAGEREntities();

            string user = HttpContext.Current.User.Identity.GetUserName();

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            int orgcode = Convert.ToInt32(codes);

            string code = orgcode.ToString();

            var orgdetails = db.Organizations.OrderByDescending(p => p.ID).FirstOrDefault(p => p.org_code == orgcode);

            string org_name = orgdetails.org_name.ToUpper();

            DateTime datet = DateTime.Now;

            string date = datet.ToString();

            string footer = "Hi:" + " " + user.ToUpper() + " " + "Organisation:" + " " + org_name + " " + "Date Of Report:" + " " + date;

            base.OnEndPage(writer, document);
            var page = document.PageSize;
            float cellHeight = document.TopMargin;

            string imageFilePath = HttpContext.Current.Server.MapPath(".") + "/Images/Contacts.jpg";
            string image3 = HttpContext.Current.Server.MapPath(".") + "/Images/citi_sm-header.jpg";

            PdfPTable table1 = new PdfPTable(2);
            table1.SpacingAfter = 10F;
            table1.TotalWidth = 560f;

            PdfPCell cell13 = new PdfPCell(new Phrase("\n"));
            cell13.Border = PdfPCell.NO_BORDER;

            PdfPCell cell14 = new PdfPCell(new Phrase("\n"));
            cell14.Border = PdfPCell.NO_BORDER;
            table1.AddCell(cell13);
            table1.AddCell(cell14);

            table1.WriteSelectedRows(0, -1, 10, page.Height - cellHeight + table1.TotalHeight, writer.DirectContent);//WriteSelectedRows(0, -1, 5, document.Top, writer.DirectContent);

            var normalFont = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 7);
            PdfPTable tabFot = new PdfPTable(new float[] { 1F });
            tabFot.SpacingAfter = 10F;
            TransactionPdf getParams = new TransactionPdf();
            var myParams = getParams.Parameters("FOOTER");
            string[] footerLines = myParams.PARAMVALUE.Replace("||", "|").Split('|');

            PdfPCell cell;
            tabFot.TotalWidth = 600F;
            cell = new PdfPCell(new Phrase("\n"));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = PdfPCell.NO_BORDER;
            tabFot.AddCell(cell);
            //foreach (var n in footerLines)
            //{
            //    cell = new PdfPCell(new Phrase(n == "" ? "\n" : n, normalFont));
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    cell.Border = PdfPCell.NO_BORDER;
            //    tabFot.AddCell(cell);
            //}

            cell = new PdfPCell(new Phrase(footer, normalFont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = PdfPCell.NO_BORDER;
            tabFot.AddCell(cell);
            tabFot.WriteSelectedRows(0, -1, 20, document.Bottom, writer.DirectContent);

        }

    }
    public class TransactionPdf
    {
        B2C_MANAGEREntities db = new B2C_MANAGEREntities();

        //private readonly Repository _repository;
        readonly Logs _logs = new Logs();
        public TransactionPdf()
        {
            //_repository = new Repository();
        }

        public bool WritePdf(string fpath, string address, string loggedUser, int orgid, string DueDate = null, string DueDate2 = null)
        {
            DateTime dtz = Convert.ToDateTime(DueDate);
            DateTime dtx = Convert.ToDateTime(DueDate2);

            string user = HttpContext.Current.User.Identity.GetUserName();

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            HttpContext.Current.Session["orgcode"] = orgid.ToString();

            //int orgcode = user_details.org_code;

            //string code = orgid.ToString();

            var orgdetails = db.Organizations.OrderByDescending(p => p.ID).FirstOrDefault(p => p.org_code == orgid);

            string org_name = orgdetails.org_name.ToUpper();

            var trans = db.Saf_Reports.Where(p => p.Org_Code == orgid && p.Completion_Time >= dtz && p.Completion_Time <= dtx).ToList();

            Regex r = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            var filename = r.Replace(user_details.UserName, String.Empty);

            //var directory = address + "/PDFs/" + filename;

            string datetime = DateTime.Now.ToString("yyyyMMddHHmmss");

            string f_name = filename + "_" + datetime + ".pdf";

            string strans = "Transactions From: " + " " + DueDate + " " + "To" + " " + DueDate2;

            
            //TempData["errormessage"] = "Reports Cannot be Found"; 
            //Session
            var appRootDir = address + f_name;
            try
            {
                //if (!Directory.Exists(directory))
                //{
                //    Directory.CreateDirectory(directory);
                //}
                if (System.IO.File.Exists(appRootDir))
                    System.IO.File.Delete(appRootDir);

                //Font Definition 
                var headerfont = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 8);
                var normalFont = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 5);
                var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6);
                var underLine = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, Font.UNDERLINE);
                CreatePdf getParams = new CreatePdf();
                var myParams = getParams.Parameters("ShortCode");
                string mypar = myParams.PARAMVALUE;

                //Fetching Needed Data
                using (var fs = new FileStream(appRootDir, FileMode.Create, FileAccess.Write, FileShare.None))
                // Creating iTextSharp.text.Document object
                using (var doc = new Document())
                {
                    doc.SetMargins(doc.LeftMargin, doc.RightMargin, doc.TopMargin + 20, doc.BottomMargin + 40);
                    // Creating iTextSharp.text.pdf.PdfWriter object
                    // It helps to write the Document to the Specified FileStream
                    using (var writer = PdfWriter.GetInstance(doc, fs))
                    {
                        //Set Header & Footer Using Page Events with the above class
                        writer.PageEvent = new PdfHeaderFooter();
                        writer.AddViewerPreference(PdfName.HIDEMENUBAR, new PdfBoolean(true));
                        writer.ViewerPreferences = PdfWriter.HideMenubar;
                        // Openning the Document
                        doc.Open();
                        // Adding a paragraph
                        // NOTE: When we want to insert text, then we've to do it through creating paragraph
                        doc.NewPage();

                        var prSpace = new Paragraph("\n");
                        var pr1 = new Paragraph("Account Holder: " + org_name, headerfont);
                        var pr2 = new Paragraph("Short Code: " + mypar, headerfont);
                        var pr3 = new Paragraph("Account: " + "Business To Customer Account", headerfont);
                        var pr4 = new Paragraph(strans, headerfont);
                        var glnodesTable = new PdfPTable(10) { WidthPercentage = 100 };
                        var widths = new float[] { 8f, 11f, 11f, 8f, 7f, 7f, 7f, 7f, 11, 11 };
                        glnodesTable.SetWidths(widths);

                        var cell1 = new PdfPCell(new Phrase("Receipt No", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell1);

                        var cell2 = new PdfPCell(new Phrase("Completion Time", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell2);

                        var cell3 = new PdfPCell(new Phrase("Initiation Time", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell3);

                        var cell4 = new PdfPCell(new Phrase("Details", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell4);

                        var cell5 = new PdfPCell(new Phrase("Transaction Status", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell5);

                        var cell6 = new PdfPCell(new Phrase("Paid In", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell6);

                        var cell7 = new PdfPCell(new Phrase("Withdrawn", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell7);

                        var cell8 = new PdfPCell(new Phrase("Balance", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell8);

                        var cell9 = new PdfPCell(new Phrase("Reason Type", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell9);

                        var cell10 = new PdfPCell(new Phrase("Other Party Info", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell10);

                        foreach (var n in trans)
                        {
                            decimal paidin = Math.Round(Convert.ToDecimal(n.PaidIn), 2);

                            decimal withdrawn = Math.Round(Convert.ToDecimal(n.Widthrawn), 2);

                            decimal balanc = Math.Round(Convert.ToDecimal(n.Balance), 2);

                            var cell11 = new PdfPCell(new Phrase(n.Receipt_No.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell11);

                            var cell12 = new PdfPCell(new Phrase(n.Completion_Time.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell12);

                            var cell13 = new PdfPCell(new Phrase(n.Initiation_Time.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell13);

                            var cell14 = new PdfPCell(new Phrase(n.Details.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell14);

                            var cell15 = new PdfPCell(new Phrase(n.Transaction_Status.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell15);

                            var cell16 = new PdfPCell(new Phrase(paidin.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell16);

                            var cell17 = new PdfPCell(new Phrase(withdrawn.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell17);

                            var cell18 = new PdfPCell(new Phrase(balanc.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell18);

                            var cell19 = new PdfPCell(new Phrase(n.Reason_Type.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell19);

                            var cell20 = new PdfPCell(new Phrase(n.Other_Info.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell20);

                        }

                        //Letter Head 

                        doc.Add(pr1);
                        doc.Add(pr2);
                        doc.Add(pr3);
                        if (trans.Count > 0)
                        {
                            doc.Add(pr4);
                            doc.Add(prSpace);
                            doc.Add(glnodesTable);
                            doc.Add(prSpace);
                        }

                        var outroTable = new PdfPTable(1) { WidthPercentage = 100 };
                        PdfPCell cell;
                        cell = new PdfPCell(new Paragraph("Thank You", normalFont));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.Border = PdfPCell.NO_BORDER;
                        outroTable.AddCell(cell);
                        doc.Close();
                    }
                }


            }
            // Catching iTextSharp.text.DocumentException if any
            catch (DocumentException de)
            {
                _logs.LogError(user, "PDF - DocumentException", "Error: " + de.Message,
               address, fpath);
            }
            // Catching System.IO.IOException if any
            catch (IOException ioe)
            {
                _logs.LogError(user, "PDF - IOException", "Error: " + ioe.Message,
              address, fpath);
            }
            catch (Exception ex)
            {
                _logs.LogError(user, "PDF - Exception", "Error: " + ex.Message,
              address, fpath);
            }
            //int u_id = Convert.ToInt32(user_details.Id);
            PDF_Loc loc = new PDF_Loc();
            loc.UserId = user_details.Id;
            loc.File_Loc = appRootDir;
            loc.File_Name = f_name;
            db.PDF_Loc.Add(loc);
            db.SaveChanges();
            return false;
        }
        public bool WriteTranPdf(string fpath, string address, string loggedUser, int orgid, int val, string DueDate = null, string DueDate2 = null)
        {
            DateTime dtz = Convert.ToDateTime(DueDate);

            DateTime dtx = Convert.ToDateTime(DueDate2);

            string user = loggedUser;

            var addres = address;

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            HttpContext.Current.Session["orgcode"] = orgid.ToString();

            int orgcode = user_details.org_code;

            string code = orgid.ToString();

            var orgdetails = db.Organizations.OrderByDescending(p => p.ID).FirstOrDefault(p => p.org_code == orgid);

            var trans = db.PaymentTrans.Where(p => p.org_code == code && p.TransDate >= dtz && p.TransDate <= dtx&& p.Approve_Status == val).ToList();

            Regex r = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);

            var filename = r.Replace(user_details.UserName, String.Empty);

            //var directory = addres + "/PDFs/" + filename;

            string datetime = DateTime.Now.ToString("yyyyMMddHHmmss");

            string f_name = filename + "_" + datetime + ".pdf";

            var appRootDir = address + f_name;

            string strans = "Transactions From: " + " " + DueDate + " " + "To" + " " + DueDate2;
            //var appRootDir = addres + filename + "_" + datetime + ".pdf";
            try
            {
                //if (!Directory.Exists(directory))
                //{
                //    Directory.CreateDirectory(directory);
                //}
                if (System.IO.File.Exists(appRootDir))
                    System.IO.File.Delete(appRootDir);
                //Font Definition 
                var headerfont = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 8);
                var normalFont = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 8);
                var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9);
                var underLine = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.UNDERLINE);
                //Fetching Needed Data

                var payment_trans = db.PaymentTrans.OrderByDescending(p => p.org_code).FirstOrDefault(p => p.org_code == code);

                using (var fs = new FileStream(appRootDir, FileMode.Create, FileAccess.Write, FileShare.None))
                // Creating iTextSharp.text.Document object
                using (var doc = new Document())
                {
                    doc.SetMargins(doc.LeftMargin, doc.RightMargin, doc.TopMargin + 20, doc.BottomMargin + 40);
                    // Creating iTextSharp.text.pdf.PdfWriter object
                    // It helps to write the Document to the Specified FileStream
                    using (var writer = PdfWriter.GetInstance(doc, fs))
                    {
                        //Set Header & Footer Using Page Events with the above class
                        writer.PageEvent = new PdfHeaderFooter();
                        writer.AddViewerPreference(PdfName.HIDEMENUBAR, new PdfBoolean(true));
                        writer.ViewerPreferences = PdfWriter.HideMenubar;
                        // Openning the Document
                        doc.Open();
                        doc.NewPage();
                        var prSpace = new Paragraph("\n");
                        var pr1 = new Paragraph("DATE: " + String.Format("{0:MMMM d, yyyy}", DateTime.Now), headerfont);
                        var pr2 = new Paragraph("TO: " + user_details.UserName.ToUpper(), headerfont);
                        var pr3 = new Paragraph("RE: " + orgdetails.org_name.ToUpper(), headerfont);
                        var pr4 = new Paragraph(strans, headerfont);
                        var glnodesTable = new PdfPTable(3) { WidthPercentage = 100 };
                        var widths = new float[] { 20f, 20f, 20f };
                        glnodesTable.SetWidths(widths);

                        var cell1 = new PdfPCell(new Phrase("Transaction Date", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell1);

                        var cell2 = new PdfPCell(new Phrase("Cheque Number", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell2);

                        var cell3 = new PdfPCell(new Phrase("Amount", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell3);


                        foreach (var n in trans)
                        {
                            decimal amount = Convert.ToDecimal(n.Amount);

                            decimal amount_p = Math.Round(Convert.ToDecimal(n.Amount), 2);

                            var cell5 = new PdfPCell(new Phrase(n.TransDate.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell5);

                            var cell6 = new PdfPCell(new Phrase(n.ChequeNo, normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell6);

                            var cell7 = new PdfPCell(new Phrase(amount_p.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell7);

                        }

                        //Letter Head 

                        doc.Add(pr1);
                        //doc.Add(prSpace);
                        doc.Add(pr2);
                        //doc.Add(prSpace);
                        doc.Add(pr3);
                        //doc.Add(prSpace);
                        if (trans.Count > 0)
                        {
                            doc.Add(pr4);
                            doc.Add(prSpace);
                            doc.Add(glnodesTable);
                            doc.Add(prSpace);
                        }

                        var outroTable = new PdfPTable(1) { WidthPercentage = 100 };
                        doc.Close();
                    }
                }


            }
            // Catching iTextSharp.text.DocumentException if any
            catch (DocumentException de)
            {
                _logs.LogError(user, "PDF - DocumentException", "Error: " + de.Message,
               addres, fpath);
            }
            // Catching System.IO.IOException if any
            catch (IOException ioe)
            {
                _logs.LogError(user, "PDF - IOException", "Error: " + ioe.Message,
              addres, fpath);
            }
            catch (Exception ex)
            {
                _logs.LogError(user, "PDF - Exception", "Error: " + ex.Message,
              addres, fpath);
            }
            PDF_Loc loc = new PDF_Loc();
            loc.UserId = user_details.Id;
            loc.File_Loc = appRootDir;
            loc.File_Name = f_name;
            db.PDF_Loc.Add(loc);
            db.SaveChanges();
            return false;
        }
        public bool WriteSaccoPdf(string fpath, string address, string loggedUser, int orgid, string DueDate = null, string DueDate2 = null)
        {
            DateTime dtz = Convert.ToDateTime(DueDate);

            DateTime dtx = Convert.ToDateTime(DueDate2);

            string user = loggedUser;

            var addres = address;

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            int orgcode = user_details.org_code;

            HttpContext.Current.Session["orgcode"] = orgid.ToString();

            string code = orgid.ToString();

            var orgdetails = db.Organizations.OrderByDescending(p => p.ID).FirstOrDefault(p => p.org_code == orgid);

            var trans = db.SaccoTrans.Where(p => p.Code == code && p.TransDate >= dtz && p.TransDate <= dtx).ToList();

            Regex r = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);

            var filename = r.Replace(user_details.UserName, String.Empty);

            //var directory = addres + "/PDFs/" + filename;

            string datetime = DateTime.Now.ToString("yyyyMMddHHmmss");

            string f_name = filename + "_" + datetime + ".pdf";

            var appRootDir = address + f_name;

            string strans = "Transactions From: " + " " + DueDate + " " + "To" + " " + DueDate2;
            //var appRootDir = addres + filename + "_" + datetime + ".pdf";
            try
            {
                //if (!Directory.Exists(directory))
                //{
                //    Directory.CreateDirectory(directory);
                //}
                if (System.IO.File.Exists(appRootDir))
                    System.IO.File.Delete(appRootDir);
                //Font Definition 
                var headerfont = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 8);
                var normalFont = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 8);
                var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9);
                var underLine = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.UNDERLINE);
                //Fetching Needed Data

                var payment_trans = db.PaymentTrans.OrderByDescending(p => p.org_code).FirstOrDefault(p => p.org_code == code);

                using (var fs = new FileStream(appRootDir, FileMode.Create, FileAccess.Write, FileShare.None))
                // Creating iTextSharp.text.Document object
                using (var doc = new Document())
                {
                    doc.SetMargins(doc.LeftMargin, doc.RightMargin, doc.TopMargin + 20, doc.BottomMargin + 40);
                    // Creating iTextSharp.text.pdf.PdfWriter object
                    // It helps to write the Document to the Specified FileStream
                    using (var writer = PdfWriter.GetInstance(doc, fs))
                    {
                        //Set Header & Footer Using Page Events with the above class
                        writer.PageEvent = new PdfHeaderFooter();
                        writer.AddViewerPreference(PdfName.HIDEMENUBAR, new PdfBoolean(true));
                        writer.ViewerPreferences = PdfWriter.HideMenubar;
                        // Openning the Document
                        doc.Open();
                        doc.NewPage();
                        var prSpace = new Paragraph("\n");
                        var pr1 = new Paragraph("DATE: " + String.Format("{0:MMMM d, yyyy}", DateTime.Now), headerfont);
                        var pr2 = new Paragraph("TO: " + loggedUser.ToUpper(), headerfont);
                        var pr3 = new Paragraph("RE: " + orgdetails.org_name.ToUpper(), headerfont);
                        var pr4 = new Paragraph(strans, headerfont);
                        var glnodesTable = new PdfPTable(4) { WidthPercentage = 100 };
                        var widths = new float[] { 20f, 20f, 20f, 20f };
                        glnodesTable.SetWidths(widths);

                        var cell1 = new PdfPCell(new Phrase("Phone Number", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell1);

                        var cell2 = new PdfPCell(new Phrase("Transaction Date", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell2);

                        var cell3 = new PdfPCell(new Phrase("Amount", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell3);

                        var cell4 = new PdfPCell(new Phrase("Status", boldFont))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                            Colspan = 1
                        };
                        glnodesTable.AddCell(cell4);


                        foreach (var n in trans)
                        {
                            decimal amount = Math.Round(Convert.ToDecimal(n.Amount), 2);

                            var cell5 = new PdfPCell(new Phrase(n.PhoneNumber.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell5);

                            var cell6 = new PdfPCell(new Phrase(n.TransDate.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell6);

                            var cell7 = new PdfPCell(new Phrase(amount.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell7);

                            var cell8 = new PdfPCell(new Phrase(n.PayStatus.ToString(), normalFont))
                            {
                                Colspan = 1
                            };
                            glnodesTable.AddCell(cell8);

                        }

                        //Letter Head 

                        doc.Add(pr1);
                        //doc.Add(prSpace);
                        doc.Add(pr2);
                        //doc.Add(prSpace);
                        doc.Add(pr3);
                        //doc.Add(prSpace);
                        if (trans.Count > 0)
                        {
                            doc.Add(pr4);
                            doc.Add(prSpace);
                            doc.Add(glnodesTable);
                            doc.Add(prSpace);
                        }

                        var outroTable = new PdfPTable(1) { WidthPercentage = 100 };
                        doc.Close();
                    }
                }


            }
            // Catching iTextSharp.text.DocumentException if any
            catch (DocumentException de)
            {
                _logs.LogError(user, "PDF - DocumentException", "Error: " + de.Message,
               addres, fpath);
            }
            // Catching System.IO.IOException if any
            catch (IOException ioe)
            {
                _logs.LogError(user, "PDF - IOException", "Error: " + ioe.Message,
              addres, fpath);
            }
            catch (Exception ex)
            {
                _logs.LogError(user, "PDF - Exception", "Error: " + ex.Message,
              addres, fpath);
            }
            PDF_Loc loc = new PDF_Loc();
            loc.UserId = user_details.Id;
            loc.File_Loc = appRootDir;
            loc.File_Name = f_name;
            db.PDF_Loc.Add(loc);
            db.SaveChanges();
            return false;
        }
        public Parameter Parameters(string paramName)
        {
            return db.Parameters.OrderByDescending(p => p.ID).FirstOrDefault(p => p.PARAMNAME == paramName);
        }


    }
}



