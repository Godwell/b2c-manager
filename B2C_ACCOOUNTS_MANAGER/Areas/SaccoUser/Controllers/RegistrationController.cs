﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using B2C_ACCOOUNTS_MANAGER.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using B2C_ACCOOUNTS_MANAGER.Functions;
using B2C_ACCOOUNTS_MANAGER.Models.UOW;

namespace B2C_ACCOOUNTS_MANAGER.Areas.SaccoUser.Controllers
{
    [Authorize]
    public class RegistrationController : Controller
    {
        //private readonly Repository _repository;
        private readonly UniqueId _uniqueId = new UniqueId();
        //private ApplicationSignInManager _signInManager;
        //private ApplicationUserManager _userManager;
        B2C_MANAGEREntities db = new B2C_MANAGEREntities();
        // GET: SaccoUser/Registration
        public ActionResult Index()
        {
            string user = Common.Classes.Common.ReturnLastUpdateby(false, Request);

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            int orgcode = user_details.org_code;

            string code = orgcode.ToString();

            var users = db.Client_Accounts.Where(p => p.Org_Code == code);

            //var usr = (from y in db.AspNetUsers
            //           where (y.org_code == orgcode && y.privilege==5)
            //           select new UserViewModel
            //           {
            //               Status_Desc = (from xx in db.Status_Options where xx.ID == y.Status select xx.Status).ToList(),
            //               Org_Name = (from yy in db.Organizations where yy.org_code == y.org_code select yy.org_name).ToList(),
            //               Id = y.Id,
            //               Names = y.Names,
            //               IDnumber = y.ID_No,
            //               AccountNumber = y.Account_No,
            //               Org_Code = y.org_code,
            //               PhoneNumber = y.PhoneNumber,
            //               Status = y.Status,
            //           }).ToList();

            return View(users);
        }
        public ActionResult Register()
        {
            B2C_MANAGEREntities db = new B2C_MANAGEREntities();
            //ViewBag.organisations = new SelectList(db.Organizations, "org_code", "org_name");
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegViewModel model)
        {
            string P_number;

            string user = Common.Classes.Common.ReturnLastUpdateby(false, Request);

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            int orgcode = user_details.org_code;

            try
            {
                var ussr = db.Client_Accounts.Any(p => p.UserID == model.UserName);

                if (ussr != false)
                {
                    TempData["errmessage"] = "User Already Exists!!!";
                    return RedirectToAction("Register");
                }
                else
                {
                    string phone = model.PhoneNumber;
                    if (phone.StartsWith("+"))
                    {
                        string phoneno = phone.Substring(1);
                        P_number = phoneno;
                    }
                    else
                    {
                        string no = phone.Substring(1);
                        string phone_no = "254" + no;
                        P_number = phone_no;
                    }
                    
                    if (ModelState.IsValid)
                    {
                        char[] charArr = "0123456789".ToCharArray();
                        //Generate a random 4 digit pin
                        string strrandom = string.Empty;
                        Random objran = new Random();
                        int noofcharacters = Convert.ToInt32(4);
                        for (int i = 0; i < noofcharacters; i++)
                        {
                            //It will not allow Repetation of Characters
                            int pos = objran.Next(1, charArr.Length);
                            if (!strrandom.Contains(charArr.GetValue(pos).ToString()))
                                strrandom += charArr.GetValue(pos);
                            else
                                i--;
                        }
                        string y = Common.Classes.Common.Encrypt(strrandom);
                        var acc = db.client_details.Any(p => p.PhoneNumber == P_number);
                        if (acc == false)
                        {
                            //string rand = Common.Classes.Common.RandomOTP()
                            client_details det = new client_details();
                            det.Org_Code = orgcode.ToString();
                            det.Account_No = model.AccountNumber;
                            det.PhoneNumber = P_number;
                            det.Names = model.Names;
                            det.PIN = y;
                            det.Clearpin = strrandom;
                            det.UserID = model.UserName;
                            det.IDNo = model.IDNO;
                            det.SendStatus = "NOT SENT";
                            det.PINStatus = "CHANGE";
                            db.client_details.Add(det);
                            db.SaveChanges();


                        }
                        else
                        {
                        }
                            var client_acc = db.Client_Accounts.Any(p => p.Org_Code == orgcode.ToString() && p.PhoneNumber == P_number && p.Account_No == model.AccountNumber);
                            if(client_acc == false)
                            {
                                Client_Accounts account = new Client_Accounts();
                                account.Org_Code = orgcode.ToString();
                                account.Account_No = model.AccountNumber;
                                account.PhoneNumber = P_number;
                                account.Names = model.Names;
                                account.UserID = model.UserName;
                                account.SendStatus = "NOT SENT";
                                db.Client_Accounts.Add(account);
                                db.SaveChanges();
                            }                      
                        TempData["shortM"] = "Created Successfuly";
                        return RedirectToAction("Index");
                    }
                }
            
            }
            catch (Exception ex)
            {

            }
            return View(model);
        }
        
        public ActionResult ResetPassword()
        {
            string usr = Common.Classes.Common.ReturnLastUpdateby(false, Request);
            ViewBag.usr = usr;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model, FormCollection form)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);

            string strName = form["txtTitle"].ToString();

            var user = await UserManager.FindAsync(strName, model.OldPassword);

            if (user != null)
            {
                string newPassword = model.Password;
                String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
                var useredit = db.AspNetUsers.FirstOrDefault(p => p.UserName == strName);
                useredit.PasswordHash = hashedNewPassword;
                db.Entry(useredit).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                TempData["reset"] = "Successfuly Reset Password";
                return RedirectToAction("Login", "Account", new { area = "" });
            }
            TempData["fail"] = "Invalid username or password";
            return RedirectToAction("ResetPassword");
        }
        public ActionResult UserActivate(string id)
        {
            try
            {
                AspNetUser user = db.AspNetUsers.Find(id);
                user.Status = (int)MyEnums.StatusOption.Approved;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();               
                TempData["shortMsg"] = "Activated Successfuly";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        public ActionResult UserDeactivate(string id)
        {
            try
            {
                AspNetUser user = db.AspNetUsers.Find(id);
                user.Status = (int)MyEnums.StatusOption.Unapproved;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                TempData["shortMessage"] = "Activated Successfuly";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        public ActionResult Delete(int id)
        {
            Client_Accounts user = db.Client_Accounts.Find(id);
            db.Client_Accounts.Remove(user);
            db.SaveChanges();
            client_details det = db.client_details.FirstOrDefault(p => p.PhoneNumber == user.PhoneNumber);
            db.client_details.Remove(det);
            db.SaveChanges();
            TempData["deletemessage"] = "Deleted successfully";
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {
            var usr_acc = db.Client_Accounts.FirstOrDefault(p => p.ID == id);
            var usr_det = db.client_details.FirstOrDefault(p => p.PhoneNumber == usr_acc.PhoneNumber);
            RegViewModel reg = new RegViewModel();
            reg.IDNO = usr_det.IDNo;
            reg.UserName = usr_acc.UserID;
            reg.Names = usr_acc.Names;
            reg.PhoneNumber = usr_acc.PhoneNumber;
            reg.AccountNumber = usr_acc.Account_No;
            return View(reg);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RegViewModel regvm)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    var usredit = db.Client_Accounts.FirstOrDefault(p => p.ID == regvm.ID);
                    usredit.Names = regvm.Names;
                    usredit.Account_No = regvm.AccountNumber;
                    usredit.UserID = regvm.UserName;
                    db.Entry(usredit).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    var usrdet = db.client_details.FirstOrDefault(p => p.PhoneNumber == regvm.PhoneNumber);
                    usrdet.UserID = regvm.UserName;
                    usrdet.Names = regvm.Names;
                    usrdet.IDNo = regvm.IDNO;
                    usrdet.Account_No = regvm.AccountNumber;
                    db.Entry(usrdet).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    //int ID = Convert.ToInt32(col["ID"]);
                    //string phone = col["PhoneNumber"].ToString();
                    //client_details det = db.client_details.Find(ID);
                    //det.UserID = col["UserID"].ToString();
                    //det.Names = col["Names"].ToString();
                    //det.Account_No = col["Account_No"].ToString();
                    //det.SendStatus = "NOT SENT";
                    //det.PINStatus = "CHANGE";
                    //db.Entry(det).State = EntityState.Modified;
                    //db.SaveChanges();

                    //Client_Accounts acc = db.Client_Accounts.OrderByDescending(p => p.ID).FirstOrDefault(p => p.PhoneNumber == phone);
                    //acc.UserID = col["UserID"].ToString();
                    //acc.Names = col["Names"].ToString();
                    //acc.Account_No = col["Account_No"].ToString();
                    //acc.SendStatus = "NOT SENT";
                    //db.Entry(acc).State = EntityState.Modified;
                    //db.SaveChanges();
                    TempData["shortMessage"] = "Edited Successsfully";
                    return RedirectToAction("Index");
                }
                catch(Exception ex)
                {
                    ex.Data.Clear();
                }
                
            }
            return View();
        }
       
    }
}