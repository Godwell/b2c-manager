﻿using System.Web.Mvc;

namespace B2C_ACCOOUNTS_MANAGER.Areas.SaccoUser
{
    public class SaccoUserAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SaccoUser";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SaccoUser_default",
                "SaccoUser/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}