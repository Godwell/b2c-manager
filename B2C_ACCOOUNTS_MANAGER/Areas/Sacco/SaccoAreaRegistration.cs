﻿using System.Web.Mvc;

namespace B2C_ACCOOUNTS_MANAGER.Areas.Sacco
{
    public class SaccoAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Sacco";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Sacco_default",
                "Sacco/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}