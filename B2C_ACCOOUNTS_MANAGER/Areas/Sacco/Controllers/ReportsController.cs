﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using B2C_ACCOOUNTS_MANAGER.Models;
using System.Globalization;
using Microsoft.AspNet.Identity.EntityFramework;
using B2C_ACCOOUNTS_MANAGER.Functions;
using B2C_ACCOOUNTS_MANAGER.Models.UOW;
using B2C_ACCOOUNTS_MANAGER.PDF;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Font = iTextSharp.text.Font;
using Image = iTextSharp.text.Image;
using System.Text.RegularExpressions;

namespace B2C_ACCOOUNTS_MANAGER.Areas.Sacco.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        readonly Logs _logs = new Logs();
        private readonly Repository _repository;
        private readonly UniqueId _uniqueId = new UniqueId();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        B2C_MANAGEREntities db = new B2C_MANAGEREntities();


        // GET: Sacco/Reports
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SafReports()
        {
            return View();
        }
        public ActionResult GetSafPDF(string DueDate = null, string DueDate2 = null)
        {
            string dateone = DueDate + " " + "00:00:00";

            string datetwo = DueDate2 + " " + "23:59:59";

            DateTime dtz = Convert.ToDateTime(dateone);

            DateTime dtx = Convert.ToDateTime(datetwo);

            string user = Common.Classes.Common.ReturnLastUpdateby(false, Request);

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            int orgcode = user_details.org_code;

            string code = orgcode.ToString();

            var trans = db.Saf_Reports.Where(p => p.Org_Code == orgcode && p.Completion_Time >= dtz && p.Completion_Time <= dtx).ToList();

            if(trans.Count ==0)
            {
                TempData["errormessage"] = "Reports Cannot be Found";
                return RedirectToAction("SafReports");
            }
            else
            {
                string fpath = Request.ServerVariables["REMOTE_ADDR"];

                var addres = Path.Combine(Server.MapPath("~/Files/"));

                var create_pdf = new CreatePdf();

                create_pdf.WritePdf(fpath, addres, user, dateone, datetwo);

                var file_loc = db.PDF_Loc.OrderByDescending(p => p.ID).FirstOrDefault(p => p.UserId == user_details.Id);

                //Response.AppendHeader("Content-Disposition", "inline; filename=" + file_loc.File_Name + ";");

                //string path = AppDomain.CurrentDomain.BaseDirectory + "App_Data/";

                //return File(file_loc.File_Loc, System.Net.Mime.MediaTypeNames.Application.Pdf, file_loc.File_Name);

                //Response.AppendHeader()
                //return RedirectToAction("Checkpdf");
                var pathToTheFile = file_loc.File_Loc;
                var filestream = new FileStream(pathToTheFile, FileMode.Open, FileAccess.Read);

                return new FileStreamResult(filestream, "application/pdf");
            }

        }     
        public ActionResult PaymentReports()
        {
            return View();
        }
        public ActionResult PrintTransactions(FormCollection form, string DueDate = null, string DueDate2 = null)
        {
            string value = form["Status"].ToString();

            int val = Convert.ToInt32(value);

            string dateone = DueDate + " " + "00:00:00";

            string datetwo = DueDate2 + " " + "23:59:59";

            DateTime dtz = Convert.ToDateTime(dateone);

            DateTime dtx = Convert.ToDateTime(datetwo);

            string user = Common.Classes.Common.ReturnLastUpdateby(false, Request);

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            int orgcode = user_details.org_code;

            string code = orgcode.ToString();

            var trans = db.PaymentTrans.Where(p => p.org_code == code && p.TransDate >= dtz && p.TransDate <= dtx && p.Approve_Status == val).ToList();

            if(trans.Count == 0)
            {
                TempData["errormessage"] = "Reports Cannot be Found";
                return RedirectToAction("PaymentReports");
            }
            else
            {
                string fpath = Request.ServerVariables["REMOTE_ADDR"];

                var addres = Path.Combine(Server.MapPath("~/Files/"));

                var paymentreport = new CreatePdf();

                paymentreport.WriteTranPdf(fpath, addres, user, val, dateone, datetwo);

                var file_loc = db.PDF_Loc.OrderByDescending(p => p.ID).FirstOrDefault(p => p.UserId == user_details.Id);

                var pathToTheFile = file_loc.File_Loc;
                var filestream = new FileStream(pathToTheFile, FileMode.Open, FileAccess.Read);

                return new FileStreamResult(filestream, "application/pdf");
            }
            
            //return RedirectToAction("Checkpdf");
        }
        public ActionResult SaccoReports()
        {
            return View();
        }
        public ActionResult SaccoTransactions(string DueDate = null, string DueDate2 = null)
        {
            
            string dateone = DueDate + " " + "00:00:00";

            string datetwo = DueDate2 + " " + "23:59:59";

            DateTime dtz = Convert.ToDateTime(dateone);

            DateTime dtx = Convert.ToDateTime(datetwo);

            string user = Common.Classes.Common.ReturnLastUpdateby(false, Request);

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            int orgcode = user_details.org_code;

            string code = orgcode.ToString();

            var trans = db.SaccoTrans.Where(p => p.Code == code && p.TransDate >= dtz && p.TransDate <= dtx).ToList();

            if(trans.Count == 0)
            {
                TempData["errormessage"] = "Reports Cannot be Found";
                return RedirectToAction("SaccoReports");
            }
            else
            {
                string fpath = Request.ServerVariables["REMOTE_ADDR"];

                var addres = Path.Combine(Server.MapPath("~/Files/"));

                var paymentreport = new CreatePdf();

                paymentreport.WriteSaccoPdf(fpath, addres, user, dateone, datetwo);

                var file_loc = db.PDF_Loc.OrderByDescending(p => p.ID).FirstOrDefault(p => p.UserId == user_details.Id);

                var pathToTheFile = file_loc.File_Loc;
                var filestream = new FileStream(pathToTheFile, FileMode.Open, FileAccess.Read);

                return new FileStreamResult(filestream, "application/pdf");
            }
            
        }
    }
}