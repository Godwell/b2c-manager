﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using B2C_ACCOOUNTS_MANAGER.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using B2C_ACCOOUNTS_MANAGER.Functions;
using B2C_ACCOOUNTS_MANAGER.Models.UOW;

namespace B2C_ACCOOUNTS_MANAGER.Areas.Sacco.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly Repository _repository;
        private readonly UniqueId _uniqueId = new UniqueId();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        B2C_MANAGEREntities db = new B2C_MANAGEREntities();
        // GET: Sacco/User
        public ActionResult Index()
        {
            string user = Common.Classes.Common.ReturnLastUpdateby(false, Request);

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            int orgcode = user_details.org_code;

            var users = db.AspNetUsers.Where(p=>p.org_code == orgcode);

            var usr = (from y in db.AspNetUsers
                       where (y.org_code == orgcode && y.privilege == 4)
                       select new UserViewModel
                       {
                           Status_Desc = (from xx in db.Status_Options where xx.ID == y.Status select xx.Status).ToList(),
                           Org_Name = (from yy in db.Organizations where yy.org_code == y.org_code select yy.org_name).ToList(),
                           Id = y.Id,
                           Names = y.Names,
                           IDnumber = y.ID_No,
                           Org_Code = y.org_code,
                           PhoneNumber = y.PhoneNumber,
                           Status = y.Status,
                       }).ToList().OrderByDescending(c => c.Id);


            return View(usr);
        }
        public ActionResult Register()
        {
            B2C_MANAGEREntities db = new B2C_MANAGEREntities();
            ViewBag.organisations = new SelectList(db.Organizations, "org_code", "org_name");
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            string P_number;

            string user = Common.Classes.Common.ReturnLastUpdateby(false, Request);

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            int orgcode = user_details.org_code;

            //var users = db.AspNetUsers.Where(p => p.org_code == orgcode);

            try
            {
                var ussr = db.AspNetUsers.Any(p => p.UserName == model.UserName);

                if (ussr != false)
                {
                    TempData["errmessage"] = "User Already Exists!!!";
                    return RedirectToAction("Register");
                }
                else
                {
                    string phone = model.PhoneNumber;
                    if (phone.StartsWith("+"))
                    {
                        string phoneno = phone.Substring(1);
                        P_number = phoneno;
                    }
                    else
                    {
                        string no = phone.Substring(1);
                        string phone_no = "254" + no;
                        P_number = phone_no;
                    }

                    ApplicationDbContext context = new ApplicationDbContext();
                    UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
                    UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
                    string newPassword = model.Password;
                    String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
                    if (ModelState.IsValid)
                    {
                        AspNetUser usr = new AspNetUser();
                        Guid id = _uniqueId.GetId();
                        usr.Id = id.ToString();
                        usr.Names = model.Names;
                        usr.UserName = model.UserName;
                        usr.PhoneNumber = P_number;
                        usr.ID_No = model.IDNO;
                        usr.org_code = orgcode;
                        usr.Email = model.Email;
                        usr.PasswordHash = hashedNewPassword;
                        usr.SecurityStamp = _uniqueId.GetId().ToString();
                        usr.Status = (int)MyEnums.StatusOption.Added;
                        usr.privilege = 4;
                        db.AspNetUsers.Add(usr);
                        db.SaveChanges();
                        TempData["shortM"] = "Created Successfuly";
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {

            }


            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult ResetPassword()
        {
            string usr = Common.Classes.Common.ReturnLastUpdateby(false, Request);
            ViewBag.usr = usr;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model, FormCollection form)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);

            string strName = form["txtTitle"].ToString();

            var user = await UserManager.FindAsync(strName, model.OldPassword);

            if (user != null)
            {
                string newPassword = model.Password;
                String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
                    var useredit = db.AspNetUsers.FirstOrDefault(p => p.UserName == strName);
                    useredit.PasswordHash = hashedNewPassword;
                    db.Entry(useredit).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    TempData["reset"] = "Successfuly Reset Password";
                    return RedirectToAction("Login", "Account", new { area = "" });
            }
            TempData["fail"] = "Invalid username or password";
            return RedirectToAction("ResetPassword");
        }

        public ActionResult UserActivate(string id)
        {
            try
            {
                AspNetUser user = db.AspNetUsers.Find(id);
                user.Status = (int)MyEnums.StatusOption.Approved;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                TempData["shortMsg"] = "Activated Successfuly";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        public ActionResult UserDeactivate(string id)
        {
            try
            {
                AspNetUser user = db.AspNetUsers.Find(id);
                user.Status = (int)MyEnums.StatusOption.Unapproved;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                TempData["shortMessage"] = "Deactivated Successfuly";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        public ActionResult Delete(string id)
        {
            AspNetUser user = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(user);
            db.SaveChanges();
            TempData["deletemessage"] = "Deleted successfully";
            return RedirectToAction("Index");
        }
    }
}