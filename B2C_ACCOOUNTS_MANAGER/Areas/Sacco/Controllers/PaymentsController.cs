﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Mvc;
using B2C_ACCOOUNTS_MANAGER.Functions;
using B2C_ACCOOUNTS_MANAGER.Models;
using B2C_ACCOOUNTS_MANAGER.Models.UOW;
using System.Drawing;

namespace B2C_ACCOOUNTS_MANAGER.Areas.Sacco.Controllers
{
    [Authorize]
    public class PaymentsController : Controller
    {
        private readonly Repository _repository;
        private readonly UniqueId _uniqueId = new UniqueId();
        private const int PageSize = 25;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        B2C_MANAGEREntities db = new B2C_MANAGEREntities();
        // GET: Sacco/Payments
        public ActionResult Index()
        {
            string user = Common.Classes.Common.ReturnLastUpdateby(false, Request);

            var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

            string orgcode = Convert.ToString(user_details.org_code);

            //var payments = db.PaymentTrans.Where(p => p.Approve_Status != 3 && p.org_code == orgcode).ToList();

            var pp = (from x in db.PaymentTrans
                      where (x.Approve_Status != 3 && x.org_code == orgcode)
                      select new PaymentViewModel
                      {
                          Status_Desc = (from xx in db.Status_Options where xx.ID == x.Approve_Status select xx.Status).ToList(),
                          ID = x.ID,
                          orgcode = x.org_code,
                          TransDate = x.TransDate,
                          //ChequeNo = x.ChequeNo,
                          Amount = x.Amount,
                          Status = x.Approve_Status,
                      }).ToList().OrderByDescending(c => c.ID);

            int o_code = Convert.ToInt32(orgcode);

            var bal = db.AmountBals.Single(x => x.org_code == o_code);

            decimal amount = bal.balance;

            decimal smscost1 = Math.Round(Convert.ToDecimal(amount), 2);

            ViewBag.bal = smscost1;

            return View(pp);
        }

        public ActionResult Create()
        {
            ViewBag.organisations = new SelectList(db.Organizations, "org_code", "org_name");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PaymentViewModel model, HttpPostedFileBase Chequeimg)
        {
            if (ModelState.IsValid)
            {
                string chequeno = "";

                string user = Common.Classes.Common.ReturnLastUpdateby(false, Request);

                var user_details = db.AspNetUsers.OrderByDescending(p => p.Id).FirstOrDefault(p => p.UserName == user);

                string orgcode = Convert.ToString(user_details.org_code);

                string sysname = "ProT_" + Common.Classes.Common.ReturnDateTimeSingleString();
                try
                {
                    //generate random receipt no
                    char[] charArr = "0123456789".ToCharArray();

                    string strrandom = string.Empty;
                    Random objran = new Random();
                    int noofcharacters = Convert.ToInt32(4);
                    for (int i = 0; i < noofcharacters; i++)
                    {
                        //It will not allow Repetation of Characters
                        int pos = objran.Next(1, charArr.Length);
                        if (!strrandom.Contains(charArr.GetValue(pos).ToString()))
                            strrandom += charArr.GetValue(pos);
                        else
                            i--;
                    }
                    string str_random = "KE" + "-" + strrandom;
                    if (Chequeimg != null)
                    {
                        int clength = Chequeimg.ContentLength;
                        byte[] data = new byte[Chequeimg.ContentLength];
                        Chequeimg.InputStream.Read(data, 0, Chequeimg.ContentLength);
                        //var trans = db.PaymentTrans.Any(p => p.ChequeNo == model.ChequeNo);
                        var paymenttran = new PaymentTran();
                        //if (!db.PaymentTrans.Any(p => p.ChequeNo == model.ChequeNo))
                        //{
                        paymenttran.org_code = orgcode;
                        paymenttran.TransDate = DateTime.Now;
                        //paymenttran.ChequeNo = model.ChequeNo;
                        paymenttran.Amount = model.Amount;
                        paymenttran.Chequeimg = data;
                        paymenttran.ChequeNo = str_random;
                        paymenttran.Approve_Status = Convert.ToInt32(MyEnums.StatusOption.Added);
                        db.PaymentTrans.Add(paymenttran);
                        TempData["shortM"] = "Created Successfuly";
                        db.SaveChanges();
                        
                    }
                    else
                    {
                        //var img
                        var paymenttran = new PaymentTran();                        
                        paymenttran.org_code = orgcode;
                        paymenttran.TransDate = DateTime.Now;
                        paymenttran.Amount = model.Amount;
                        paymenttran.ChequeNo = str_random;
                        paymenttran.Approve_Status = Convert.ToInt32(MyEnums.StatusOption.Added);
                        db.PaymentTrans.Add(paymenttran);
                        TempData["shortM"] = "Created Successfuly";
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {

                }
                //TempData["shortMessage"] = "Successfully Created";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["shortMessage"] = "Amount Field Is Required";
                return RedirectToAction("Create");
            }
        }

    }
}